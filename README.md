# squadra-filmoteca-react

**Web App/React para o catálogo de filmes**

> https://squadra-filmoteca.firebaseapp.com/

### Tecnologias

* NodeJs
* React
* typescript
* redux
* redux-Sagas
* immer
* styled-components

### Observações

O Web app está hospedado no **[firebase](https://firebase.google.com/)**
