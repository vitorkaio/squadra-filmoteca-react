import { Movie, MovieAdd, MovieEdit } from 'models/Movie.model'

export interface StateProps {
  moviesData: Movie[] | undefined
  moviesLoadingData: boolean
  moviesErrorData: boolean
  moviesErrorMsgData: string

  movieAddSucessData: boolean
  movieAddLoadingData: boolean
  movieAddErrorData: boolean
  movieAddErrorMsgData: string

  movieRemoveSucessData: boolean
  movieRemoveLoadingData: boolean
  movieRemoveErrorData: boolean
  movieRemoveErrorMsgData: string

  movieEditSucessData: boolean
  movieEditLoadingData: boolean
  movieEditErrorData: boolean
  movieEditErrorMsgData: string
}

export interface DispatchProps {
  movieRequest(): void

  movieAddRequest(newMovie: MovieAdd): void
  movieAddReset(): void

  movieRemoveRequest(id: number): void
  movieRemoveReset(): void

  movieEditRequest(newMovie: MovieEdit): void
  movieEditReset(): void
}
