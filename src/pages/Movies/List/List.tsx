import React from 'react'
import {
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Tooltip
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import ListIcon from '@material-ui/icons/ListAlt'
import MovieImg from 'components/styles/assets/movie.svg'

// import { Container } from './styles';
import { Movie } from 'models/Movie.model'

import { StateProps, DispatchProps } from './ListTypes'

type Props = StateProps & DispatchProps

const ListMovies: React.FC<Props> = props => {
  const { movies, removeMovie, editMovie, openManagerModal } = props

  const generate = () => {
    return movies?.map((movie: Movie) => {
      return (
        <ListItem key={movie.id}>
          <ListItemAvatar>
            <img src={MovieImg} alt="movie" width={40} height={40} />
          </ListItemAvatar>
          <ListItemText primary={movie.name} />
          <ListItemSecondaryAction>
            <Tooltip title="Elenco e Diretores">
              <IconButton
                edge="end"
                aria-label="list"
                onClick={() => openManagerModal(movie.id)}
              >
                <ListIcon color="action" />
              </IconButton>
            </Tooltip>
            <Tooltip title="Editar Filme">
              <IconButton
                edge="end"
                aria-label="edit"
                onClick={() => editMovie(movie)}
              >
                <EditIcon color="action" />
              </IconButton>
            </Tooltip>
            <Tooltip title="Deletar Filme">
              <IconButton
                edge="end"
                aria-label="delete"
                onClick={() => removeMovie(movie)}
              >
                <DeleteIcon color="error" />
              </IconButton>
            </Tooltip>
          </ListItemSecondaryAction>
        </ListItem>
      )
    })
  }
  return (
    <List style={{ width: 500 }}>{movies !== undefined && generate()}</List>
  )
}

export default ListMovies
