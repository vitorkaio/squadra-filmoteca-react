import { Movie } from 'models/Movie.model'

export interface StateProps {
  movies: Movie[] | undefined
}

export interface DispatchProps {
  removeMovie(movie: Movie): void
  editMovie(movie: Movie): void
  openManagerModal(id: number): void
}
