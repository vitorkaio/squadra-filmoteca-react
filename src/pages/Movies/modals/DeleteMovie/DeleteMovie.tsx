import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  CircularProgress
} from '@material-ui/core'
import ErrorComponent from 'components/Error/Error'
import { Container, Text } from './DeleteMovieStyle'
import { StateProps, DispatchProps } from './DeleteMovieTypes'

type Props = StateProps & DispatchProps

const DeleteMovie: React.FC<Props> = props => {
  const {
    openRemoveMovie,
    confirmDelete,
    name,
    loading,
    error,
    errorMsg
  } = props

  const getRender = () => {
    if (loading) {
      return <CircularProgress />
    }
    if (error) {
      return <ErrorComponent error={errorMsg} />
    }
    return <Text>{`Deseja deletar ${name}?`}</Text>
  }

  return (
    <Dialog
      open={openRemoveMovie}
      onClose={() => confirmDelete(false)}
      aria-labelledby="form-dialog-title"
      disableBackdropClick
      disableEscapeKeyDown
    >
      <DialogTitle id="form-dialog-title">Deletar Filme</DialogTitle>
      <DialogContent>
        <Container>{getRender()}</Container>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => confirmDelete(false)} color="primary">
          Cancelar
        </Button>
        <Button
          onClick={() => confirmDelete(true)}
          color="primary"
          disabled={name.length === 0 || loading}
        >
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default DeleteMovie
