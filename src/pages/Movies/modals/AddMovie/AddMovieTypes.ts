import { MovieAdd, MovieEdit } from 'models/Movie.model'

export interface StateProps {
  openAddMovie: boolean
  loading: boolean
  success: boolean
  error: boolean
  errorMsg: string
  editMovie: MovieEdit | undefined
}

export interface DispatchProps {
  handlerClickAddMovieOpen(): void
  submit(newMovie: MovieAdd | MovieEdit): void
}
