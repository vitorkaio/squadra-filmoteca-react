import React, { useState, useEffect } from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  CircularProgress
} from '@material-ui/core'
import { MovieAdd, MovieEdit } from 'models/Movie.model'
import { Container, ActionButtonOk } from './AddMovieStyle'
import { StateProps, DispatchProps } from './AddMovieTypes'
import AddMovieForms from './Forms/Forms'

type Props = StateProps & DispatchProps

const AddMovie: React.FC<Props> = props => {
  const {
    openAddMovie,
    handlerClickAddMovieOpen,
    submit,
    loading,
    success,
    error,
    errorMsg,
    editMovie
  } = props

  const [name, setName] = useState('')

  const onSubmit = () => {
    if (name.length !== 0) {
      if (editMovie) {
        const newMovie: MovieEdit = { ...editMovie, name }
        submit(newMovie)
      } else {
        const newMovie: MovieAdd = { name }
        submit(newMovie)
      }
    }
  }

  const cancelAction = () => {
    handlerClickAddMovieOpen()
    setName('')
  }

  useEffect(() => {
    if (success) {
      setName('')
    }
  }, [success])

  return (
    <Dialog
      open={openAddMovie}
      onClose={handlerClickAddMovieOpen}
      aria-labelledby="form-dialog-title"
      disableBackdropClick
      disableEscapeKeyDown
    >
      <DialogTitle id="form-dialog-title">
        {editMovie ? 'Editar Filme' : 'Adicionar Filme'}
      </DialogTitle>
      <DialogContent>
        <Container>
          <AddMovieForms
            onInputText={(text: string) => setName(text)}
            movieError={error}
            movieErrorMsg={errorMsg}
            edit={!!editMovie}
            editName={editMovie?.name}
          />
        </Container>
      </DialogContent>
      <DialogActions>
        <Button onClick={cancelAction} color="primary">
          Cancelar
        </Button>
        <ActionButtonOk>
          {loading ? (
            <CircularProgress size={25} />
          ) : (
            <Button
              onClick={onSubmit}
              color="primary"
              disabled={name.length === 0 || loading}
            >
              {editMovie ? 'Editar' : 'Adicionar'}
            </Button>
          )}
        </ActionButtonOk>
      </DialogActions>
    </Dialog>
  )
}

export default AddMovie
