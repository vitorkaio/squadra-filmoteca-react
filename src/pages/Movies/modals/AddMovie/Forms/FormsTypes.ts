export interface DispatchProps {
  edit?: boolean
  editName?: string
  movieError: boolean
  movieErrorMsg: string
  onInputText: (name: string) => void
}

export interface InputDataType {
  data?: string
  error: boolean
  errorMsg: string
}

export interface StateProps {
  nameState?: InputDataType
}
