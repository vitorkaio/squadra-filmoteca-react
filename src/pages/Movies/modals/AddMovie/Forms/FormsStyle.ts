import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const ErrorMsg = styled.div`
  margin-top: 0.1rem;
  color: coral;
`
