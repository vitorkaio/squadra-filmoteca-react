/* eslint-disable react/jsx-props-no-spreading */
import React, { ChangeEvent } from 'react'
import {
  TextField,
  IconButton,
  CircularProgress,
  Button
} from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { Add } from '@material-ui/icons'

import { Actor } from 'models/Actor.model'
import {
  Container,
  Header,
  ActionButtonHeader,
  ManagerLoadingWrapper,
  DeleteErrorWrapper,
  TextError
} from './ActorsManagerStyle'
import { StateProps, DispatchProps } from './ActorsManagerTypes'
import ActorsListManager from './List/List'

type Props = StateProps & DispatchProps

const ActorsManager: React.FC<Props> = props => {
  const {
    actors,
    managerActorOfMovie,
    movieActors,
    managerActorLoading,
    managerActorError,
    managerActorErrorMsg,
    managerActorReset
  } = props

  let actor: Actor | null = null

  const addActorHandler = (event: ChangeEvent<{}>, value: Actor | null) => {
    actor = value
  }

  const addActorInMovieHandler = () => {
    if (actor) {
      managerActorOfMovie(true, !actor?.id ? 0 : actor.id)
      actor = null
    }
  }

  const removeActorOfMovieHandler = (removeActor: Actor) => {
    managerActorOfMovie(false, removeActor.id)
  }

  // Só mostra os atores que não foram cadastrados ainda
  const getOptions = (): Actor[] | undefined => {
    return (
      actors &&
      actors?.filter((actorItem: Actor) => {
        return !movieActors?.find(
          (actorFind: Actor) => actorFind.id === actorItem.id
        )
      })
    )
  }

  const getRender = () => {
    if (managerActorLoading) {
      return (
        <ManagerLoadingWrapper>
          <CircularProgress size={45} />
        </ManagerLoadingWrapper>
      )
    }
    if (managerActorError) {
      return (
        <DeleteErrorWrapper>
          <TextError>{managerActorErrorMsg}</TextError>
          <Button
            color="primary"
            variant="outlined"
            style={{ marginTop: 16 }}
            onClick={managerActorReset}
          >
            Voltar
          </Button>
        </DeleteErrorWrapper>
      )
    }
    return (
      <ActorsListManager
        actors={!movieActors ? undefined : movieActors}
        removeActorOfMovie={removeActorOfMovieHandler}
      />
    )
  }

  return (
    <Container>
      <Header>
        <Autocomplete
          disabled={!actors}
          id="combo-box-demo"
          options={getOptions()}
          getOptionLabel={option => option.name}
          onChange={addActorHandler}
          value={actor}
          style={{ width: '70%' }}
          renderInput={params => (
            <TextField
              {...params}
              label={!actors ? 'Carregando lista...' : 'Escolha um Ator'}
              variant="outlined"
              fullWidth
              size="small"
            />
          )}
        />
        <ActionButtonHeader>
          {!actors ? (
            <CircularProgress size={20} />
          ) : (
            <IconButton
              aria-label="add"
              color="primary"
              onClick={addActorInMovieHandler}
              disabled={managerActorLoading}
            >
              <Add />
            </IconButton>
          )}
        </ActionButtonHeader>
      </Header>
      {getRender()}
    </Container>
  )
}

export default ActorsManager
