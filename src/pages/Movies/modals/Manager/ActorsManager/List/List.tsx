import React from 'react'
import {
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  IconButton
} from '@material-ui/core'
import { Delete, LabelImportant } from '@material-ui/icons'

import { Actor } from 'models/Actor.model'
import { Container, Content } from './ListStyle'
import { StateProps, DispatchProps } from './ListTypes'

type Props = StateProps & DispatchProps

const ListActorsManager: React.FC<Props> = props => {
  const { actors, removeActorOfMovie } = props

  const removeActor = (actor: Actor) => {
    removeActorOfMovie(actor)
  }

  const generate = () => {
    return actors?.map((actor: Actor) => {
      return (
        <ListItem key={actor.id} button>
          <ListItemAvatar>
            <LabelImportant />
          </ListItemAvatar>

          <ListItemText primary={actor.name} />
          <ListItemSecondaryAction>
            <IconButton
              edge="end"
              aria-label="delete"
              onClick={() => removeActor(actor)}
            >
              <Delete color="error" />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      )
    })
  }

  return (
    <Container>
      <Content>
        <List style={{ width: '90%' }}>
          {actors !== undefined && generate()}
        </List>
      </Content>
    </Container>
  )
}

export default ListActorsManager
