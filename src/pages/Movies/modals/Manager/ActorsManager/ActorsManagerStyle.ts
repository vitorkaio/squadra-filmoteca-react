import styled from 'styled-components'
import { Colors } from 'components/styles/Colors'

export const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`

export const Header = styled.div`
  padding: 10px 2px;

  display: flex;
  justify-content: center;
  align-items: center;
`

export const ActionButtonHeader = styled.div`
  margin-left: 0.5rem;
`

export const ManagerLoadingWrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const DeleteErrorWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const TextError = styled.span`
  margin-top: 0.3rem;
  color: ${Colors.error};
  font-weight: 450;
  text-align: center;
`
