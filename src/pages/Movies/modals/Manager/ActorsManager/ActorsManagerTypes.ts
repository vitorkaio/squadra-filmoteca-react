import { Actor } from 'models/Actor.model'

export interface StateProps {
  actors: Actor[] | undefined
  movieActors: Actor[] | undefined

  managerActorLoading: boolean
  managerActorError: boolean
  managerActorErrorMsg: string
}

export interface DispatchProps {
  managerActorOfMovie(op: boolean, id: number): void
  managerActorReset(): void
}
