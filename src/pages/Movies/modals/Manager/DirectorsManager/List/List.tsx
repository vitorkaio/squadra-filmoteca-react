import React from 'react'
import {
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  IconButton
} from '@material-ui/core'
import { Delete, LabelImportant } from '@material-ui/icons'

import { Director } from 'models/Director.model'
import { Container, Content } from './ListStyle'
import { StateProps, DispatchProps } from './ListTypes'

type Props = StateProps & DispatchProps

const ListDirectorsManager: React.FC<Props> = props => {
  const { directors, removeDirectorOfMovie } = props

  const removeDirector = (director: Director) => {
    removeDirectorOfMovie(director)
  }

  const generate = () => {
    return directors?.map((director: Director) => {
      return (
        <ListItem key={director.id} button>
          <ListItemAvatar>
            <LabelImportant />
          </ListItemAvatar>

          <ListItemText primary={director.name} />
          <ListItemSecondaryAction>
            <IconButton
              edge="end"
              aria-label="delete"
              onClick={() => removeDirector(director)}
            >
              <Delete color="error" />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      )
    })
  }

  return (
    <Container>
      <Content>
        <List style={{ width: '90%' }}>
          {directors !== undefined && generate()}
        </List>
      </Content>
    </Container>
  )
}

export default ListDirectorsManager
