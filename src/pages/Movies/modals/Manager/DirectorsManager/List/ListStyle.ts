import styled from 'styled-components'

export const Container = styled.div`
  flex: 1;
  width: 100%;
`

export const Content = styled.div`
  width: 100%;
  height: 360px;
  overflow-y: auto;
  display: flex;
  justify-content: center;
  @media (min-height: 767px) {
    height: 500px;
  }
`
