import { Director } from 'models/Director.model'

export interface StateProps {
  directors: Director[] | undefined
}

export interface DispatchProps {
  removeDirectorOfMovie(director: Director): void
}
