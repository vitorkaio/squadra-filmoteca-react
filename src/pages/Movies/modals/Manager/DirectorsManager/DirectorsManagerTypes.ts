import { Director } from 'models/Director.model'

export interface StateProps {
  directors: Director[] | undefined
  movieDirectors: Director[] | undefined

  managerDirectorLoading: boolean
  managerDirectorError: boolean
  managerDirectorErrorMsg: string
}

export interface DispatchProps {
  managerDirectorOfMovie(op: boolean, id: number): void
  managerDirectorReset(): void
}
