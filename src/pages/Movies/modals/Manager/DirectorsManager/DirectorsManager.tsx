/* eslint-disable react/jsx-props-no-spreading */
import React, { ChangeEvent } from 'react'
import {
  TextField,
  IconButton,
  CircularProgress,
  Button
} from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { Add } from '@material-ui/icons'

import { Director } from 'models/Director.model'
import {
  Container,
  Header,
  ActionButtonHeader,
  ManagerLoadingWrapper,
  DeleteErrorWrapper,
  TextError
} from './DirectorsManagerStyle'
import { StateProps, DispatchProps } from './DirectorsManagerTypes'
import DirectorsListManager from './List/List'

type Props = StateProps & DispatchProps

const DirectorsManager: React.FC<Props> = props => {
  const {
    directors,
    managerDirectorOfMovie,
    movieDirectors,
    managerDirectorLoading,
    managerDirectorError,
    managerDirectorErrorMsg,
    managerDirectorReset
  } = props

  let director: Director | null = null

  const addDirectorHandler = (
    event: ChangeEvent<{}>,
    value: Director | null
  ) => {
    director = value
  }

  const addDirectorInMovieHandler = () => {
    if (director) {
      managerDirectorOfMovie(true, !director?.id ? 0 : director.id)
      director = null
    }
  }

  const removeDirectorOfMovieHandler = (removeDirector: Director) => {
    managerDirectorOfMovie(false, removeDirector.id)
  }

  // Só mostra os atores que não foram cadastrados ainda
  const getOptions = (): Director[] | undefined => {
    return (
      directors &&
      directors?.filter((directorItem: Director) => {
        return !movieDirectors?.find(
          (directorFind: Director) => directorFind.id === directorItem.id
        )
      })
    )
  }

  const getRender = () => {
    if (managerDirectorLoading) {
      return (
        <ManagerLoadingWrapper>
          <CircularProgress size={45} />
        </ManagerLoadingWrapper>
      )
    }
    if (managerDirectorError) {
      return (
        <DeleteErrorWrapper>
          <TextError>{managerDirectorErrorMsg}</TextError>
          <Button
            color="primary"
            variant="outlined"
            style={{ marginTop: 16 }}
            onClick={managerDirectorReset}
          >
            Voltar
          </Button>
        </DeleteErrorWrapper>
      )
    }
    return (
      <DirectorsListManager
        directors={!movieDirectors ? undefined : movieDirectors}
        removeDirectorOfMovie={removeDirectorOfMovieHandler}
      />
    )
  }

  return (
    <Container>
      <Header>
        <Autocomplete
          disabled={!directors}
          id="combo-box-demo-director"
          options={getOptions()}
          getOptionLabel={option => option.name}
          onChange={addDirectorHandler}
          value={director}
          style={{ width: '70%' }}
          renderInput={params => (
            <TextField
              {...params}
              label={!directors ? 'Carregando lista...' : 'Escolha um Diretor'}
              variant="outlined"
              fullWidth
              size="small"
            />
          )}
        />
        <ActionButtonHeader>
          {!directors ? (
            <CircularProgress size={20} />
          ) : (
            <IconButton
              aria-label="add"
              color="primary"
              onClick={addDirectorInMovieHandler}
              disabled={managerDirectorLoading}
            >
              <Add />
            </IconButton>
          )}
        </ActionButtonHeader>
      </Header>
      {getRender()}
    </Container>
  )
}

export default DirectorsManager
