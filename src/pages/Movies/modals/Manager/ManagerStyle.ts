import styled from 'styled-components'
import { Colors } from 'components/styles/Colors'

export const Container = styled.div`
  width: 100%;
  height: 100vh;

  display: flex;
  justify-content: center;
  align-items: center;
`

export const Content = styled.div`
  width: 810px;
  max-width: 92%;
  height: 700px;
  max-height: 90%;
  background-color: white;

  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`

export const Header = styled.div`
  width: 100%;
  height: 46px;
  border-bottom: 1px solid ${Colors.border};

  display: flex;
  justify-content: center;
  align-items: center;
`

export const HeaderTitle = styled.span`
  font-size: 1rem;
  font-weight: 550;
  color: ${Colors.text};
`

export const Footer = styled.div`
  width: 100%;
  height: 46px;

  display: flex;
  justify-content: flex-end;
  align-items: center;

  border-top: 1px solid ${Colors.border};
`

export const FooterActionButton = styled.div`
  margin-right: 1rem;
`

export const ManagerContent = styled.div`
  flex: 1;
  display: flex;
`

export const ActorWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;

  border-right: 1px solid ${Colors.border};
`

export const DirectorWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`
