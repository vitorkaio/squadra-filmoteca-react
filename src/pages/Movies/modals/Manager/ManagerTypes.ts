import {
  Movie,
  ManagerActorMovie,
  ManagerDirectorMovie
} from 'models/Movie.model'
import { Actor } from 'models/Actor.model'
import { Director } from 'models/Director.model'

export interface StateProps {
  managerOpenModal: boolean
  idMovie: number
  moviesData: Movie[] | undefined

  deleteActorMovieLoadingData: boolean
  deleteActorMovieErrorData: boolean
  deleteActorMovieErrorMsgData: string

  addActorMovieLoadingData: boolean
  addActorMovieErrorData: boolean
  addActorMovieErrorMsgData: string

  actorsData: Actor[] | undefined

  deleteDirectorMovieLoadingData: boolean
  deleteDirectorMovieErrorData: boolean
  deleteDirectorMovieErrorMsgData: string

  addDirectorMovieLoadingData: boolean
  addDirectorMovieErrorData: boolean
  addDirectorMovieErrorMsgData: string

  directorsData: Director[] | undefined
}

export interface DispatchProps {
  handlerManagerModal(id: number): void

  actorRequest(): void

  deleteActorMovieRequest(infoData: ManagerActorMovie): void
  deleteActorMovieReset(): void

  addActorMovieRequest(infoData: ManagerActorMovie): void
  addActorMovieReset(): void

  directorRequest(): void

  deleteDirectorMovieRequest(infoData: ManagerDirectorMovie): void
  deleteDirectorMovieReset(): void

  addDirectorMovieRequest(infoData: ManagerDirectorMovie): void
  addDirectorMovieReset(): void
}
