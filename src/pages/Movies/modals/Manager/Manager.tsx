// Gerencia a inserção e remoção de atores e diretores
import React, { useEffect, useState } from 'react'
import { Modal, Button } from '@material-ui/core'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { ApplicationState } from 'store/store'
import * as MovieActions from 'store/modules/movies/actions'
import * as ActorActions from 'store/modules/actors/actions'
import * as DirectorActions from 'store/modules/directors/actions'

import {
  Movie,
  ManagerActorMovie,
  ManagerDirectorMovie
} from 'models/Movie.model'
import {
  Container,
  Content,
  Header,
  HeaderTitle,
  ManagerContent,
  ActorWrapper,
  DirectorWrapper,
  Footer,
  FooterActionButton
} from './ManagerStyle'
import { StateProps, DispatchProps } from './ManagerTypes'
import ActorsManager from './ActorsManager/ActorsManager'
import DirectorsManager from './DirectorsManager/DirectorsManager'

type Props = StateProps & DispatchProps

const Manager: React.FC<Props> = props => {
  const {
    managerOpenModal,
    handlerManagerModal,
    idMovie,
    moviesData,
    actorRequest,
    actorsData,
    deleteActorMovieRequest,
    deleteActorMovieLoadingData,
    deleteActorMovieErrorData,
    deleteActorMovieErrorMsgData,
    deleteActorMovieReset,
    addActorMovieRequest,
    addActorMovieLoadingData,
    addActorMovieErrorData,
    addActorMovieErrorMsgData,
    addActorMovieReset,

    directorRequest,
    directorsData,
    deleteDirectorMovieRequest,
    deleteDirectorMovieLoadingData,
    deleteDirectorMovieErrorData,
    deleteDirectorMovieErrorMsgData,
    deleteDirectorMovieReset,
    addDirectorMovieRequest,
    addDirectorMovieLoadingData,
    addDirectorMovieErrorData,
    addDirectorMovieErrorMsgData,
    addDirectorMovieReset
  } = props

  const [managerActor, setManagerActor] = useState(false)
  const [managerDirector, setManagerDirector] = useState(false)

  const closeModal = () => {
    if (addActorMovieErrorData) {
      addActorMovieReset()
    }
    if (deleteActorMovieErrorData) {
      deleteActorMovieReset()
    }
    if (addDirectorMovieErrorData) {
      addDirectorMovieReset()
    }
    if (deleteDirectorMovieErrorData) {
      deleteDirectorMovieReset()
    }
    handlerManagerModal(0)
  }

  const getMovie = (): Movie | undefined => {
    return moviesData?.filter((mv: Movie) => mv.id === idMovie).pop()
  }

  const managerActorOfMovie = (op: boolean, idActor: number) => {
    setManagerActor(op)
    const movie: Movie | undefined = getMovie()
    if (movie) {
      const infoData: ManagerActorMovie = {
        idMovie: movie?.id,
        idActor
      }
      if (op) {
        addActorMovieRequest(infoData)
      } else {
        deleteActorMovieRequest(infoData)
      }
    }
  }

  const managerDirectorOfMovie = (op: boolean, idDirector: number) => {
    setManagerDirector(op)
    const movie: Movie | undefined = getMovie()
    if (movie) {
      const infoData: ManagerDirectorMovie = {
        idMovie: movie?.id,
        idDirector
      }
      if (op) {
        addDirectorMovieRequest(infoData)
      } else {
        deleteDirectorMovieRequest(infoData)
      }
    }
  }

  useEffect(() => {
    // Se os atores já foram carregados no redux, não precisa fazer req de novo
    if (!actorsData) {
      actorRequest()
    }
    if (!directorsData) {
      directorRequest()
    }
  }, [])

  return (
    <Modal
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      open={managerOpenModal}
      onClose={closeModal}
    >
      <Container>
        <Content>
          <Header>
            <HeaderTitle>{getMovie()?.name}</HeaderTitle>
          </Header>
          <ManagerContent>
            <ActorWrapper>
              <ActorsManager
                actors={actorsData}
                movieActors={getMovie()?.actors}
                managerActorOfMovie={managerActorOfMovie}
                managerActorLoading={
                  managerActor
                    ? addActorMovieLoadingData
                    : deleteActorMovieLoadingData
                }
                managerActorError={
                  managerActor
                    ? addActorMovieErrorData
                    : deleteActorMovieErrorData
                }
                managerActorErrorMsg={
                  managerActor
                    ? addActorMovieErrorMsgData
                    : deleteActorMovieErrorMsgData
                }
                managerActorReset={
                  managerActor ? addActorMovieReset : deleteActorMovieReset
                }
              />
            </ActorWrapper>
            <DirectorWrapper>
              <DirectorsManager
                directors={directorsData}
                movieDirectors={getMovie()?.directors}
                managerDirectorOfMovie={managerDirectorOfMovie}
                managerDirectorLoading={
                  managerDirector
                    ? addDirectorMovieLoadingData
                    : deleteDirectorMovieLoadingData
                }
                managerDirectorError={
                  managerDirector
                    ? addDirectorMovieErrorData
                    : deleteDirectorMovieErrorData
                }
                managerDirectorErrorMsg={
                  managerDirector
                    ? addDirectorMovieErrorMsgData
                    : deleteDirectorMovieErrorMsgData
                }
                managerDirectorReset={
                  managerDirector
                    ? addDirectorMovieReset
                    : deleteDirectorMovieReset
                }
              />
            </DirectorWrapper>
          </ManagerContent>
          <Footer>
            <FooterActionButton>
              <Button color="primary" onClick={closeModal}>
                Fechar
              </Button>
            </FooterActionButton>
          </Footer>
        </Content>
      </Container>
    </Modal>
  )
}

const mapStateToProps = (state: ApplicationState) => ({
  moviesData: state.movieReducer.movies,

  deleteActorMovieLoadingData: state.movieReducer.deleteActorMovieLoading,
  deleteActorMovieErrorData: state.movieReducer.deleteActorMovieError,
  deleteActorMovieErrorMsgData: state.movieReducer.deleteActorMovieErrorMsg,

  addActorMovieLoadingData: state.movieReducer.addActorMovieLoading,
  addActorMovieErrorData: state.movieReducer.addActorMovieError,
  addActorMovieErrorMsgData: state.movieReducer.addActorMovieErrorMsg,

  actorsData: state.actorReducer.actors,

  deleteDirectorMovieLoadingData: state.movieReducer.deleteDirectorMovieLoading,
  deleteDirectorMovieErrorData: state.movieReducer.deleteDirectorMovieError,
  deleteDirectorMovieErrorMsgData:
    state.movieReducer.deleteDirectorMovieErrorMsg,

  addDirectorMovieLoadingData: state.movieReducer.addDirectorMovieLoading,
  addDirectorMovieErrorData: state.movieReducer.addDirectorMovieError,
  addDirectorMovieErrorMsgData: state.movieReducer.addDirectorMovieErrorMsg,

  directorsData: state.directorReducer.directors
})

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    { ...MovieActions, ...ActorActions, ...DirectorActions },
    dispatch
  )

export default connect(mapStateToProps, mapDispatchToProps)(Manager)
