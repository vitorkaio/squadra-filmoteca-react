import styled from 'styled-components'

export const Container = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  position: relative;
`

export const Content = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  height: 500px;
  overflow-y: auto;
`

export const LoadingData = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`
