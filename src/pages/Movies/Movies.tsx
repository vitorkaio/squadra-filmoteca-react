import React, { useState, useEffect } from 'react'
import { CircularProgress } from '@material-ui/core'

import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { ApplicationState } from 'store/store'
import * as MovieActions from 'store/modules/movies/actions'

import AddButton from 'components/AddButton/AddButton'
import { MovieAdd, Movie, MovieEdit } from 'models/Movie.model'
import ErrorComponent from 'components/Error/Error'
import { Container, LoadingData, Content } from './MoviesStyle'
import { StateProps, DispatchProps } from './MoviesTypes'
import ListMovies from './List/List'
import AddMovieModal from './modals/AddMovie/AddMovie'
import DeleteMovieModal from './modals/DeleteMovie/DeleteMovie'
import ManagerModal from './modals/Manager/Manager'

type Props = StateProps & DispatchProps

const Movies: React.FC<Props> = props => {
  const {
    movieRequest,
    moviesData,
    moviesLoadingData,
    moviesErrorData,
    moviesErrorMsgData,
    movieAddRequest,
    movieAddReset,
    movieAddLoadingData,
    movieAddSucessData,
    movieAddErrorData,
    movieAddErrorMsgData,
    movieRemoveRequest,
    movieRemoveLoadingData,
    movieRemoveSucessData,
    movieRemoveErrorData,
    movieRemoveErrorMsgData,
    movieRemoveReset,
    movieEditRequest,
    movieEditReset,
    movieEditLoadingData,
    movieEditSucessData,
    movieEditErrorData,
    movieEditErrorMsgData
  } = props

  const [addMovieOpen, setAddMovieOpen] = useState(false)
  const [deleteMovieOpen, setDeleteMovieOpen] = useState(false)
  const [movieDelete, setMovieDelete] = useState<Movie>({ id: 0, name: '' })
  const [movieEdit, setMovieEdit] = useState<MovieEdit | undefined>()
  const [managerModal, setManagerModal] = useState(0)

  useEffect(() => {
    movieRequest()
  }, [])

  const addMovieHandler = () => {
    setAddMovieOpen(!addMovieOpen)
    if (movieAddErrorData || movieAddSucessData) {
      movieAddReset()
    }
    if (movieEditErrorData || movieEditSucessData) {
      movieEditReset()
    }
    if (movieEdit) {
      setMovieEdit(undefined)
    }
  }

  const deleteMovieHandler = () => {
    setDeleteMovieOpen(!deleteMovieOpen)
    if (movieRemoveErrorData || movieRemoveSucessData) {
      movieRemoveReset()
      setMovieDelete({ id: 0, name: '' })
    }
  }

  const sumbit = (newMovie: MovieAdd | MovieEdit) => {
    if (movieEdit) {
      movieEditRequest(newMovie as MovieEdit)
    } else {
      movieAddRequest(newMovie)
    }
  }

  const removeMovie = (movie: Movie) => {
    setMovieDelete(movie)
    deleteMovieHandler()
  }

  const confirmDeleteMovie = (op: boolean) => {
    if (op) {
      movieRemoveRequest(movieDelete.id)
    } else {
      deleteMovieHandler()
    }
  }

  const editMovieHandler = (movie: Movie) => {
    const newMovieEdit: MovieEdit = { id: movie.id, name: movie.name }
    setMovieEdit(newMovieEdit)
    addMovieHandler()
  }

  const openManagerModal = (id: number) => {
    if (id !== 0) {
      setManagerModal(id)
    } else {
      setManagerModal(0)
    }
  }

  useEffect(() => {
    if (movieAddSucessData || movieEditSucessData) {
      addMovieHandler()
    }
  }, [movieAddSucessData, movieEditSucessData])

  useEffect(() => {
    if (movieRemoveSucessData) {
      deleteMovieHandler()
    }
  }, [movieRemoveSucessData])

  const getRender = () => {
    if (moviesLoadingData) {
      return (
        <LoadingData>
          <CircularProgress />
        </LoadingData>
      )
    }
    if (moviesErrorData) {
      return <ErrorComponent error={moviesErrorMsgData} />
    }

    return (
      <ListMovies
        movies={moviesData}
        removeMovie={removeMovie}
        editMovie={editMovieHandler}
        openManagerModal={openManagerModal}
      />
    )
  }

  return (
    <Container>
      <Content>{getRender()}</Content>
      <AddButton action={addMovieHandler} />
      {addMovieOpen ? (
        <AddMovieModal
          loading={movieEdit ? movieEditLoadingData : movieAddLoadingData}
          success={movieEdit ? movieEditSucessData : movieAddSucessData}
          error={movieEdit ? movieEditErrorData : movieAddErrorData}
          errorMsg={movieEdit ? movieEditErrorMsgData : movieAddErrorMsgData}
          openAddMovie={addMovieOpen}
          handlerClickAddMovieOpen={addMovieHandler}
          submit={sumbit}
          editMovie={movieEdit}
        />
      ) : null}
      {deleteMovieOpen ? (
        <DeleteMovieModal
          loading={movieRemoveLoadingData}
          error={movieRemoveErrorData}
          errorMsg={movieRemoveErrorMsgData}
          confirmDelete={confirmDeleteMovie}
          name={movieDelete.name}
          openRemoveMovie={deleteMovieOpen}
        />
      ) : null}
      {managerModal !== 0 ? (
        <ManagerModal
          managerOpenModal={managerModal !== 0}
          idMovie={managerModal}
          handlerManagerModal={openManagerModal}
        />
      ) : null}
    </Container>
  )
}

const mapStateToProps = (state: ApplicationState) => ({
  moviesData: state.movieReducer.movies,
  moviesLoadingData: state.movieReducer.moviesLoading,
  moviesErrorData: state.movieReducer.moviesError,
  moviesErrorMsgData: state.movieReducer.moviesErrorMsg,

  movieAddSucessData: state.movieReducer.movieAddSuccess,
  movieAddLoadingData: state.movieReducer.movieAddLoading,
  movieAddErrorData: state.movieReducer.movieAddError,
  movieAddErrorMsgData: state.movieReducer.movieAddErrorMsg,

  movieRemoveSucessData: state.movieReducer.movieRemoveSuccess,
  movieRemoveLoadingData: state.movieReducer.movieRemoveLoading,
  movieRemoveErrorData: state.movieReducer.movieRemoveError,
  movieRemoveErrorMsgData: state.movieReducer.movieRemoveErrorMsg,

  movieEditSucessData: state.movieReducer.movieEditSuccess,
  movieEditLoadingData: state.movieReducer.movieEditLoading,
  movieEditErrorData: state.movieReducer.movieEditError,
  movieEditErrorMsgData: state.movieReducer.movieEditErrorMsg
})

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ ...MovieActions }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Movies)
