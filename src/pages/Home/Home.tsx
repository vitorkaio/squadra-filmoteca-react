/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react'
import { Tabs, Tab, Paper } from '@material-ui/core'

import Movies from 'pages/Movies/Movies'
import Actors from 'pages/Actors/Actors'
import Directors from 'pages/Directors/Directors'

import { Container, Header, TabContent, Wrapper } from './HomeStyle'

const Home: React.FC = () => {
  const [value, setValue] = useState(0)

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue)
  }

  const getPage = () => {
    if (value === 1) {
      return <Actors />
    }
    if (value === 2) {
      return <Directors />
    }

    return <Movies />
  }

  return (
    <Container>
      <Wrapper>
        <Header>
          <Paper square elevation={0}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="simple"
              indicatorColor="primary"
              textColor="primary"
              centered
            >
              <Tab
                label="Filmes"
                id={`simple-tab-${0}`}
                aria-controls={`simple-tabpanel-${0}`}
              />
              <Tab
                label="Atores"
                id={`simple-tab-${1}`}
                aria-controls={`simple-tabpanel-${1}`}
              />
              <Tab
                label="Diretores"
                id={`simple-tab-${2}`}
                aria-controls={`simple-tabpanel-${2}`}
              />
            </Tabs>
          </Paper>
        </Header>
        <TabContent>{getPage()}</TabContent>
      </Wrapper>
    </Container>
  )
}

export default Home
