import styled from 'styled-components'

export const Container = styled.div`
  width: 100%;
  height: 100vh;

  display: flex;
  justify-content: center;
  align-items: center;
`

export const Wrapper = styled.div`
  width: 810px;
  max-width: 92%;
  height: 700px;
  max-height: 90%;
  display: flex;
  flex-direction: column;

  box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
  padding: 0.3rem;
`

export const Header = styled.div``

export const TabContent = styled.div`
  flex: 1;
  display: flex;
  margin-top: 0.5rem;
`
