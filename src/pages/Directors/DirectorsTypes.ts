import { Director, DirectorAdd } from 'models/Director.model'

export interface StateProps {
  directorsData: Director[] | undefined
  directorsLoadingData: boolean
  directorsErrorData: boolean
  directorsErrorMsgData: string

  directorAddSucessData: boolean
  directorAddLoadingData: boolean
  directorAddErrorData: boolean
  directorAddErrorMsgData: string

  directorRemoveSucessData: boolean
  directorRemoveLoadingData: boolean
  directorRemoveErrorData: boolean
  directorRemoveErrorMsgData: string
}

export interface DispatchProps {
  directorRequest(): void

  directorAddRequest(newDirector: DirectorAdd): void
  directorAddReset(): void

  directorRemoveRequest(id: number): void
  directorRemoveReset(): void
}
