import { DirectorAdd } from 'models/Director.model'

export interface StateProps {
  openAddDirector: boolean
  loading: boolean
  error: boolean
  errorMsg: string
}

export interface DispatchProps {
  handlerClickAddDirectorOpen(): void
  submit(newDirector: DirectorAdd): void
}
