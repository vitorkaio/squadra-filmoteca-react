import React, { useState } from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  CircularProgress
} from '@material-ui/core'
import { DirectorAdd } from 'models/Director.model'
import { Container, ActionButtonOk } from './AddDirectorStyle'
import { StateProps, DispatchProps } from './AddDirectorTypes'
import AddDirectorForms from './Forms/Forms'

type Props = StateProps & DispatchProps

const AddDirector: React.FC<Props> = props => {
  const {
    openAddDirector,
    handlerClickAddDirectorOpen,
    submit,
    loading,
    error,
    errorMsg
  } = props

  const [name, setName] = useState('')

  const onSubmit = () => {
    if (name.length !== 0) {
      const newDirector: DirectorAdd = { name }
      submit(newDirector)
    }
  }

  const cancelAction = () => {
    handlerClickAddDirectorOpen()
    setName('')
  }

  return (
    <Dialog
      open={openAddDirector}
      onClose={handlerClickAddDirectorOpen}
      aria-labelledby="form-dialog-title"
      disableBackdropClick
      disableEscapeKeyDown
    >
      <DialogTitle id="form-dialog-title">Adicionar Diretor</DialogTitle>
      <DialogContent>
        <Container>
          <AddDirectorForms
            onInputText={(text: string) => setName(text)}
            directorError={error}
            directorErrorMsg={errorMsg}
          />
        </Container>
      </DialogContent>
      <DialogActions>
        <Button onClick={cancelAction} color="primary">
          Cancelar
        </Button>
        <ActionButtonOk>
          {loading ? (
            <CircularProgress size={25} />
          ) : (
            <Button
              onClick={onSubmit}
              color="primary"
              disabled={name.length === 0 || loading}
            >
              Adicionar
            </Button>
          )}
        </ActionButtonOk>
      </DialogActions>
    </Dialog>
  )
}

export default AddDirector
