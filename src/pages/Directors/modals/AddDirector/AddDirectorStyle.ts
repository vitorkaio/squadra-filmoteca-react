import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 250px;
  height: 80px;
`

export const ActionButtonOk = styled.div`
  width: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
`
