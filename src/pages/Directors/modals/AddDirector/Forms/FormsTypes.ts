export interface DispatchProps {
  edit?: boolean
  editName?: string
  directorError: boolean
  directorErrorMsg: string
  onInputText: (name: string) => void
}

export interface InputDataType {
  data?: string
  error: boolean
  errorMsg: string
}

export interface StateProps {
  nameState?: InputDataType
}
