/* eslint-disable no-case-declarations */
/* eslint-disable no-param-reassign */
import React, { useState, useEffect, useCallback } from 'react'
import { TextField } from '@material-ui/core'
import produce from 'immer'
import ErrorComponent from 'components/Error/Error'
import { DispatchProps, StateProps, InputDataType } from './FormsTypes'
import { Container } from './FormsStyle'

type Props = DispatchProps & StateProps

const TrainingForms: React.FC<Props> = props => {
  const { edit, editName, directorError, directorErrorMsg, onInputText } = props

  const [name, setName] = useState<InputDataType>({
    data: edit ? editName : '',
    error: false,
    errorMsg: ''
  })

  useEffect(() => {
    if (directorError) {
      setName(prev => {
        return produce(prev, draft => {
          draft.error = true
          draft.errorMsg = directorErrorMsg
        })
      })
    }
  }, [directorError, directorErrorMsg])

  const inputChangeHandler = useCallback(
    (id: string, data: string) => {
      switch (id) {
        case 'name':
          const nextDataname = produce(name, draft => {
            draft.data = data
            draft.error = false
            draft.errorMsg = ''
          })
          setName(nextDataname)
          onInputText(data)
          break

        default:
          break
      }
    },
    [name]
  )

  return (
    <Container>
      <TextField
        autoFocus
        variant="outlined"
        margin="dense"
        id="name"
        label="Nome do Diretor"
        type="text"
        fullWidth
        value={name.data}
        onChange={e => inputChangeHandler('name', e.target.value)}
      />
      {name.error && <ErrorComponent error={name.errorMsg} />}
    </Container>
  )
}

export default TrainingForms
