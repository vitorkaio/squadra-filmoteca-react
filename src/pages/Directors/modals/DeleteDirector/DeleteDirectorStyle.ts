import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 200px;
  height: 80px;
`
export const Text = styled.span`
  font-size: 0.9rem;
  font-weight: 500;
`

export const TextError = styled.span`
  margin-top: 0.3rem;
  color: coral;
  font-weight: 450;
  text-align: center;
`
