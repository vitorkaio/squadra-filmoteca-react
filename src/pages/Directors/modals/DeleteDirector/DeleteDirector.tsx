import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  CircularProgress
} from '@material-ui/core'
import ErrorComponent from 'components/Error/Error'
import { Container, Text } from './DeleteDirectorStyle'
import { StateProps, DispatchProps } from './DeleteDirectorTypes'

type Props = StateProps & DispatchProps

const DeleteDirector: React.FC<Props> = props => {
  const {
    openRemoveDirector,
    confirmDelete,
    name,
    loading,
    error,
    errorMsg
  } = props

  const getRender = () => {
    if (loading) {
      return <CircularProgress />
    }
    if (error) {
      return <ErrorComponent error={errorMsg} />
    }
    return <Text>{`Deseja deletar ${name}?`}</Text>
  }

  return (
    <Dialog
      open={openRemoveDirector}
      onClose={() => confirmDelete(false)}
      aria-labelledby="form-dialog-title"
      disableBackdropClick
      disableEscapeKeyDown
    >
      <DialogTitle id="form-dialog-title">Deletar Diretor</DialogTitle>
      <DialogContent>
        <Container>{getRender()}</Container>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => confirmDelete(false)} color="primary">
          Cancelar
        </Button>
        <Button
          onClick={() => confirmDelete(true)}
          color="primary"
          disabled={name.length === 0 || loading}
        >
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default DeleteDirector
