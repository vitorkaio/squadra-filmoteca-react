import React, { useState, useEffect } from 'react'
import { CircularProgress } from '@material-ui/core'

import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { ApplicationState } from 'store/store'
import * as DirectorActions from 'store/modules/directors/actions'

import AddButton from 'components/AddButton/AddButton'
import { DirectorAdd, Director } from 'models/Director.model'
import ErrorComponent from 'components/Error/Error'
import { Container, LoadingData, Content } from './DirectorsStyle'
import { StateProps, DispatchProps } from './DirectorsTypes'
import ListDirectors from './List/List'
import AddDirectorModal from './modals/AddDirector/AddDirector'
import DeleteDirectorModal from './modals/DeleteDirector/DeleteDirector'

type Props = StateProps & DispatchProps

const Directors: React.FC<Props> = props => {
  const {
    directorRequest,
    directorsData,
    directorsLoadingData,
    directorsErrorData,
    directorsErrorMsgData,
    directorAddRequest,
    directorAddReset,
    directorAddLoadingData,
    directorAddSucessData,
    directorAddErrorData,
    directorAddErrorMsgData,
    directorRemoveRequest,
    directorRemoveLoadingData,
    directorRemoveSucessData,
    directorRemoveErrorData,
    directorRemoveErrorMsgData,
    directorRemoveReset
  } = props

  const [addDirectorOpen, setAddDirectorOpen] = useState(false)
  const [deleteDirectorOpen, setDeleteDirectorOpen] = useState(false)
  const [directorDelete, setDirectorDelete] = useState<Director>({
    id: 0,
    name: ''
  })

  useEffect(() => {
    directorRequest()
  }, [])

  const addDirectorHandler = () => {
    setAddDirectorOpen(!addDirectorOpen)
    if (directorAddErrorData || directorAddSucessData) {
      directorAddReset()
    }
  }

  const deleteDirectorHandler = () => {
    setDeleteDirectorOpen(!deleteDirectorOpen)
    if (directorRemoveErrorData || directorRemoveSucessData) {
      directorRemoveReset()
      setDirectorDelete({ id: 0, name: '' })
    }
  }

  const sumbit = (newDirector: DirectorAdd) => {
    directorAddRequest(newDirector)
  }

  const removeDirector = (director: Director) => {
    setDirectorDelete(director)
    deleteDirectorHandler()
  }

  const confirmDeleteDirector = (op: boolean) => {
    if (op) {
      directorRemoveRequest(directorDelete.id)
    } else {
      deleteDirectorHandler()
    }
  }

  useEffect(() => {
    if (directorAddSucessData) {
      addDirectorHandler()
    }
  }, [directorAddSucessData])

  useEffect(() => {
    if (directorRemoveSucessData) {
      deleteDirectorHandler()
    }
  }, [directorRemoveSucessData])

  const getRender = () => {
    if (directorsLoadingData) {
      return (
        <LoadingData>
          <CircularProgress />
        </LoadingData>
      )
    }
    if (directorsErrorData) {
      return <ErrorComponent error={directorsErrorMsgData} />
    }

    return (
      <ListDirectors
        directors={directorsData}
        removeDirector={removeDirector}
      />
    )
  }

  return (
    <Container>
      <Content>{getRender()}</Content>
      <AddButton action={addDirectorHandler} />
      {addDirectorOpen ? (
        <AddDirectorModal
          loading={directorAddLoadingData}
          error={directorAddErrorData}
          errorMsg={directorAddErrorMsgData}
          openAddDirector={addDirectorOpen}
          handlerClickAddDirectorOpen={addDirectorHandler}
          submit={sumbit}
        />
      ) : null}
      {deleteDirectorOpen ? (
        <DeleteDirectorModal
          loading={directorRemoveLoadingData}
          error={directorRemoveErrorData}
          errorMsg={directorRemoveErrorMsgData}
          confirmDelete={confirmDeleteDirector}
          name={directorDelete.name}
          openRemoveDirector={deleteDirectorOpen}
        />
      ) : null}
    </Container>
  )
}

const mapStateToProps = (state: ApplicationState) => ({
  directorsData: state.directorReducer.directors,
  directorsLoadingData: state.directorReducer.directorsLoading,
  directorsErrorData: state.directorReducer.directorsError,
  directorsErrorMsgData: state.directorReducer.directorsErrorMsg,

  directorAddSucessData: state.directorReducer.directorAddSuccess,
  directorAddLoadingData: state.directorReducer.directorAddLoading,
  directorAddErrorData: state.directorReducer.directorAddError,
  directorAddErrorMsgData: state.directorReducer.directorAddErrorMsg,

  directorRemoveSucessData: state.directorReducer.directorRemoveSuccess,
  directorRemoveLoadingData: state.directorReducer.directorRemoveLoading,
  directorRemoveErrorData: state.directorReducer.directorRemoveError,
  directorRemoveErrorMsgData: state.directorReducer.directorRemoveErrorMsg
})

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ ...DirectorActions }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Directors)
