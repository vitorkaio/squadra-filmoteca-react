import React from 'react'
import {
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  IconButton
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import DirectorImg from 'components/styles/assets/director.svg'

// import { Container } from './styles';
import { Director } from 'models/Director.model'

import { StateProps, DispatchProps } from './ListTypes'

type Props = StateProps & DispatchProps

const ListDirectors: React.FC<Props> = props => {
  const { directors, removeDirector } = props

  const generate = () => {
    return directors?.map((director: Director) => {
      return (
        <ListItem key={director.id}>
          <ListItemAvatar>
            <img src={DirectorImg} alt="director" width={40} height={40} />
          </ListItemAvatar>
          <ListItemText primary={director.name} />
          <ListItemSecondaryAction>
            <IconButton
              edge="end"
              aria-label="delete"
              onClick={() => removeDirector(director)}
            >
              <DeleteIcon color="error" />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      )
    })
  }
  return (
    <List style={{ width: 450 }}>{directors !== undefined && generate()}</List>
  )
}

export default ListDirectors
