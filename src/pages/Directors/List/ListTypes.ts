import { Director } from 'models/Director.model'

export interface StateProps {
  directors: Director[] | undefined
}

export interface DispatchProps {
  removeDirector(director: Director): void
}
