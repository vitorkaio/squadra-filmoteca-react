import { Actor, ActorAdd } from 'models/Actor.model'

export interface StateProps {
  actorsData: Actor[] | undefined
  actorsLoadingData: boolean
  actorsErrorData: boolean
  actorsErrorMsgData: string

  actorAddSucessData: boolean
  actorAddLoadingData: boolean
  actorAddErrorData: boolean
  actorAddErrorMsgData: string

  actorRemoveSucessData: boolean
  actorRemoveLoadingData: boolean
  actorRemoveErrorData: boolean
  actorRemoveErrorMsgData: string
}

export interface DispatchProps {
  actorRequest(): void

  actorAddRequest(newActor: ActorAdd): void
  actorAddReset(): void

  actorRemoveRequest(id: number): void
  actorRemoveReset(): void
}
