export interface StateProps {
  openRemoveActor: boolean
  loading: boolean
  error: boolean
  errorMsg: string
  name: string
}

export interface DispatchProps {
  confirmDelete(op: boolean): void
}
