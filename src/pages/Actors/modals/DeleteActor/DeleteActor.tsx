import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  CircularProgress
} from '@material-ui/core'
import ErrorComponent from 'components/Error/Error'
import { Container, Text } from './DeleteActorStyle'
import { StateProps, DispatchProps } from './DeleteActorTypes'

type Props = StateProps & DispatchProps

const DeleteActor: React.FC<Props> = props => {
  const {
    openRemoveActor,
    confirmDelete,
    name,
    loading,
    error,
    errorMsg
  } = props

  const getRender = () => {
    if (loading) {
      return <CircularProgress />
    }
    if (error) {
      return <ErrorComponent error={errorMsg} />
    }
    return <Text>{`Deseja deletar ${name}?`}</Text>
  }

  return (
    <Dialog
      open={openRemoveActor}
      onClose={() => confirmDelete(false)}
      aria-labelledby="form-dialog-title"
      disableBackdropClick
      disableEscapeKeyDown
    >
      <DialogTitle id="form-dialog-title">Deletar Ator</DialogTitle>
      <DialogContent>
        <Container>{getRender()}</Container>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => confirmDelete(false)} color="primary">
          Cancelar
        </Button>
        <Button
          onClick={() => confirmDelete(true)}
          color="primary"
          disabled={name.length === 0 || loading}
        >
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default DeleteActor
