import { ActorAdd } from 'models/Actor.model'

export interface StateProps {
  openAddActor: boolean
  loading: boolean
  error: boolean
  errorMsg: string
}

export interface DispatchProps {
  handlerClickAddActorOpen(): void
  submit(newActor: ActorAdd): void
}
