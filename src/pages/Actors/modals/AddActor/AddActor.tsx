import React, { useState } from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  CircularProgress
} from '@material-ui/core'
import { ActorAdd } from 'models/Actor.model'
import { Container, ActionButtonOk } from './addActorStyle'
import { StateProps, DispatchProps } from './AddActorTypes'
import AddActorForms from './Forms/Forms'

type Props = StateProps & DispatchProps

const AddActor: React.FC<Props> = props => {
  const {
    openAddActor,
    handlerClickAddActorOpen,
    submit,
    loading,
    error,
    errorMsg
  } = props

  const [name, setName] = useState('')

  const onSubmit = () => {
    if (name.length !== 0) {
      const newActor: ActorAdd = { name }
      submit(newActor)
    }
  }

  const cancelAction = () => {
    handlerClickAddActorOpen()
    setName('')
  }

  return (
    <Dialog
      open={openAddActor}
      onClose={handlerClickAddActorOpen}
      aria-labelledby="form-dialog-title"
      disableBackdropClick
      disableEscapeKeyDown
    >
      <DialogTitle id="form-dialog-title">Adicionar Ator</DialogTitle>
      <DialogContent>
        <Container>
          <AddActorForms
            onInputText={(text: string) => setName(text)}
            actorError={error}
            actorErrorMsg={errorMsg}
          />
        </Container>
      </DialogContent>
      <DialogActions>
        <Button onClick={cancelAction} color="primary">
          Cancelar
        </Button>
        <ActionButtonOk>
          {loading ? (
            <CircularProgress size={25} />
          ) : (
            <Button
              onClick={onSubmit}
              color="primary"
              disabled={name.length === 0 || loading}
            >
              Adicionar
            </Button>
          )}
        </ActionButtonOk>
      </DialogActions>
    </Dialog>
  )
}

export default AddActor
