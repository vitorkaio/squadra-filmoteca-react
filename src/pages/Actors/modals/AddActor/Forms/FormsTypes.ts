export interface DispatchProps {
  edit?: boolean
  editName?: string
  actorError: boolean
  actorErrorMsg: string
  onInputText: (name: string) => void
}

export interface InputDataType {
  data?: string
  error: boolean
  errorMsg: string
}

export interface StateProps {
  nameState?: InputDataType
}
