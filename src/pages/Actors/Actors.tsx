import React, { useState, useEffect } from 'react'
import { CircularProgress } from '@material-ui/core'

import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { ApplicationState } from 'store/store'
import * as ActorActions from 'store/modules/actors/actions'

import AddButton from 'components/AddButton/AddButton'
import { ActorAdd, Actor } from 'models/Actor.model'
import ErrorComponent from 'components/Error/Error'
import { Container, LoadingData, Content } from './ActorsStyle'
import { StateProps, DispatchProps } from './ActorsTypes'
import ListActors from './List/List'
import AddActorModal from './modals/AddActor/AddActor'
import DeleteActorModal from './modals/DeleteActor/DeleteActor'

type Props = StateProps & DispatchProps

const Actors: React.FC<Props> = props => {
  const {
    actorRequest,
    actorsData,
    actorsLoadingData,
    actorsErrorData,
    actorsErrorMsgData,
    actorAddRequest,
    actorAddReset,
    actorAddLoadingData,
    actorAddSucessData,
    actorAddErrorData,
    actorAddErrorMsgData,
    actorRemoveRequest,
    actorRemoveLoadingData,
    actorRemoveSucessData,
    actorRemoveErrorData,
    actorRemoveErrorMsgData,
    actorRemoveReset
  } = props

  const [addActorOpen, setAddActorOpen] = useState(false)
  const [deleteActorOpen, setDeleteActorOpen] = useState(false)
  const [actorDelete, setActorDelete] = useState<Actor>({ id: 0, name: '' })

  useEffect(() => {
    actorRequest()
  }, [])

  const addActorHandler = () => {
    setAddActorOpen(!addActorOpen)
    if (actorAddErrorData || actorAddSucessData) {
      actorAddReset()
    }
  }

  const deleteActorHandler = () => {
    setDeleteActorOpen(!deleteActorOpen)
    if (actorRemoveErrorData || actorRemoveSucessData) {
      actorRemoveReset()
      setActorDelete({ id: 0, name: '' })
    }
  }

  const sumbit = (newActor: ActorAdd) => {
    actorAddRequest(newActor)
  }

  const removeActor = (actor: Actor) => {
    setActorDelete(actor)
    deleteActorHandler()
  }

  const confirmDeleteActor = (op: boolean) => {
    if (op) {
      actorRemoveRequest(actorDelete.id)
    } else {
      deleteActorHandler()
    }
  }

  useEffect(() => {
    if (actorAddSucessData) {
      addActorHandler()
    }
  }, [actorAddSucessData])

  useEffect(() => {
    if (actorRemoveSucessData) {
      deleteActorHandler()
    }
  }, [actorRemoveSucessData])

  const getRender = () => {
    if (actorsLoadingData) {
      return (
        <LoadingData>
          <CircularProgress />
        </LoadingData>
      )
    }
    if (actorsErrorData) {
      return <ErrorComponent error={actorsErrorMsgData} />
    }

    return <ListActors actors={actorsData} removeActor={removeActor} />
  }

  return (
    <Container>
      <Content>{getRender()}</Content>
      <AddButton action={addActorHandler} />
      {addActorOpen ? (
        <AddActorModal
          loading={actorAddLoadingData}
          error={actorAddErrorData}
          errorMsg={actorAddErrorMsgData}
          openAddActor={addActorOpen}
          handlerClickAddActorOpen={addActorHandler}
          submit={sumbit}
        />
      ) : null}
      {deleteActorOpen ? (
        <DeleteActorModal
          loading={actorRemoveLoadingData}
          error={actorRemoveErrorData}
          errorMsg={actorRemoveErrorMsgData}
          confirmDelete={confirmDeleteActor}
          name={actorDelete.name}
          openRemoveActor={deleteActorOpen}
        />
      ) : null}
    </Container>
  )
}

const mapStateToProps = (state: ApplicationState) => ({
  actorsData: state.actorReducer.actors,
  actorsLoadingData: state.actorReducer.actorsLoading,
  actorsErrorData: state.actorReducer.actorsError,
  actorsErrorMsgData: state.actorReducer.actorsErrorMsg,

  actorAddSucessData: state.actorReducer.actorAddSuccess,
  actorAddLoadingData: state.actorReducer.actorAddLoading,
  actorAddErrorData: state.actorReducer.actorAddError,
  actorAddErrorMsgData: state.actorReducer.actorAddErrorMsg,

  actorRemoveSucessData: state.actorReducer.actorRemoveSuccess,
  actorRemoveLoadingData: state.actorReducer.actorRemoveLoading,
  actorRemoveErrorData: state.actorReducer.actorRemoveError,
  actorRemoveErrorMsgData: state.actorReducer.actorRemoveErrorMsg
})

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ ...ActorActions }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Actors)
