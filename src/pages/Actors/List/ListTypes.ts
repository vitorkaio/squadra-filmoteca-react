import { Actor } from 'models/Actor.model'

export interface StateProps {
  actors: Actor[] | undefined
}

export interface DispatchProps {
  removeActor(actor: Actor): void
}
