import React from 'react'
import {
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  IconButton
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import ActorImg from 'components/styles/assets/actor.svg'

// import { Container } from './styles';
import { Actor } from 'models/Actor.model'

import { StateProps, DispatchProps } from './ListTypes'

type Props = StateProps & DispatchProps

const ListActors: React.FC<Props> = props => {
  const { actors, removeActor } = props

  const generate = () => {
    return actors?.map((actor: Actor) => {
      return (
        <ListItem key={actor.id}>
          <ListItemAvatar>
            <img src={ActorImg} alt="actor" width={40} height={40} />
          </ListItemAvatar>
          <ListItemText primary={actor.name} />
          <ListItemSecondaryAction>
            <IconButton
              edge="end"
              aria-label="delete"
              onClick={() => removeActor(actor)}
            >
              <DeleteIcon color="error" />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      )
    })
  }
  return (
    <List style={{ width: 450 }}>{actors !== undefined && generate()}</List>
  )
}

export default ListActors
