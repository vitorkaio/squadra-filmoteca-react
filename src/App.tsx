import React from 'react'
import Home from 'pages/Home/Home'
import GlobalStyles from 'components/styles/Global'
import { ThemeProvider } from '@material-ui/core/styles'
import MyTheme from 'components/styles/MyTheme'
import { Provider } from 'react-redux'
import store from 'store/store'

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={MyTheme}>
        <GlobalStyles />
        <Home />
      </ThemeProvider>
    </Provider>
  )
}

export default App
