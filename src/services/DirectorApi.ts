import axios, { AxiosResponse } from 'axios'
import { Director, DirectorAdd } from 'models/Director.model'
import { ERRORS } from './FormatErrors'

const URL = 'https://polar-atoll-20679.herokuapp.com'

interface ResData {
  msg: string
  code: number
  data: Director | Director[] | string
}

interface ResErrorData {
  response: {
    data: {
      msg: string
      code: number
      data: Director | string
    }
  }
}

class DirectorApi {
  static directors = async (): Promise<Director[]> => {
    try {
      const res: AxiosResponse<ResData> = await axios.get(`${URL}/directors`)
      const directors: Director[] = res.data.data as Director[]
      return directors
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }

  // ************************************ ADD ACTOR ************************************
  static directorAdd = async (newDirector: DirectorAdd): Promise<Director> => {
    try {
      const res: AxiosResponse<ResData> = await axios.post(`${URL}/directors`, {
        ...newDirector
      })
      const director: Director = res.data.data as Director
      return director
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }

  // ************************************ Remove ACTOR ************************************
  static directorRemove = async (id: number): Promise<Director> => {
    try {
      const res: AxiosResponse<ResData> = await axios.delete(
        `${URL}/directors/${id}`
      )
      const director: Director = res.data.data as Director
      return director
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }
} // Fim da classe

export default DirectorApi
