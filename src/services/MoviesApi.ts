import axios, { AxiosResponse } from 'axios'
import {
  Movie,
  MovieAdd,
  MovieEdit,
  ManagerActorMovie,
  ManagerDirectorMovie
} from 'models/Movie.model'
import { ERRORS } from './FormatErrors'

const URL = 'https://polar-atoll-20679.herokuapp.com'

interface ResData {
  msg: string
  code: number
  data: Movie | Movie[] | string
}

interface ResErrorData {
  response: {
    data: {
      msg: string
      code: number
      data: Movie | string
    }
  }
}

class MovieApi {
  static movies = async (): Promise<Movie[]> => {
    try {
      const res: AxiosResponse<ResData> = await axios.get(`${URL}/movies`)
      const movies: Movie[] = res.data.data as Movie[]
      return movies
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }

  // ************************************ ADD MOVIE ************************************
  static movieAdd = async (newMovie: MovieAdd): Promise<Movie> => {
    try {
      const res: AxiosResponse<ResData> = await axios.post(`${URL}/movies`, {
        ...newMovie
      })
      const movie: Movie = res.data.data as Movie
      return movie
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }

  // ************************************ Remove MOVIE ************************************
  static movieRemove = async (id: number): Promise<Movie> => {
    try {
      const res: AxiosResponse<ResData> = await axios.delete(
        `${URL}/movies/${id}`
      )
      const movie: Movie = res.data.data as Movie
      return movie
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }

  // ************************************ Edit MOVIE ************************************
  static movieEdit = async (newMovie: MovieEdit): Promise<Movie> => {
    try {
      const res: AxiosResponse<ResData> = await axios.patch(
        `${URL}/movies/${newMovie.id}`,
        { name: newMovie.name }
      )
      const movie: Movie = res.data.data as Movie
      return movie
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }

  // ************************************ Delete Actor Of Movie ************************************
  static deleteActorMovie = async (
    infoData: ManagerActorMovie
  ): Promise<Movie> => {
    try {
      const res: AxiosResponse<ResData> = await axios.patch(
        `${URL}/movies/${infoData.idMovie}/actors/remove/${infoData.idActor}`
      )
      const movie: Movie = res.data.data as Movie
      return movie
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }

  // ************************************ Add Actor Of Movie ************************************
  static addActorMovie = async (
    infoData: ManagerActorMovie
  ): Promise<Movie> => {
    try {
      const res: AxiosResponse<ResData> = await axios.patch(
        `${URL}/movies/${infoData.idMovie}/actors/add/${infoData.idActor}`
      )
      const movie: Movie = res.data.data as Movie
      return movie
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }

  // ************************************ Delete Director Of Movie ************************************
  static deleteDirectorMovie = async (
    infoData: ManagerDirectorMovie
  ): Promise<Movie> => {
    try {
      const res: AxiosResponse<ResData> = await axios.patch(
        `${URL}/movies/${infoData.idMovie}/directors/remove/${infoData.idDirector}`
      )
      const movie: Movie = res.data.data as Movie
      return movie
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }

  // ************************************ Add Director Of Movie ************************************
  static addDirectorMovie = async (
    infoData: ManagerDirectorMovie
  ): Promise<Movie> => {
    try {
      const res: AxiosResponse<ResData> = await axios.patch(
        `${URL}/movies/${infoData.idMovie}/directors/add/${infoData.idDirector}`
      )
      const movie: Movie = res.data.data as Movie
      return movie
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }
} // Fim da classe

export default MovieApi
