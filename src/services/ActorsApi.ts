import axios, { AxiosResponse } from 'axios'
import { Actor, ActorAdd } from 'models/Actor.model'
import { ERRORS } from './FormatErrors'

const URL = 'https://polar-atoll-20679.herokuapp.com'

interface ResData {
  msg: string
  code: number
  data: Actor | Actor[] | string
}

interface ResErrorData {
  response: {
    data: {
      msg: string
      code: number
      data: Actor | string
    }
  }
}

class ActorApi {
  static actors = async (): Promise<Actor[]> => {
    try {
      const res: AxiosResponse<ResData> = await axios.get(`${URL}/actors`)
      const actors: Actor[] = res.data.data as Actor[]
      return actors
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }

  // ************************************ ADD ACTOR ************************************
  static actorAdd = async (newActor: ActorAdd): Promise<Actor> => {
    try {
      const res: AxiosResponse<ResData> = await axios.post(`${URL}/actors`, {
        ...newActor
      })
      const actor: Actor = res.data.data as Actor
      return actor
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }

  // ************************************ Remove ACTOR ************************************
  static actorRemove = async (id: number): Promise<Actor> => {
    try {
      const res: AxiosResponse<ResData> = await axios.delete(
        `${URL}/actors/${id}`
      )
      const actor: Actor = res.data.data as Actor
      return actor
    } catch (error) {
      const err: ResErrorData = error
      throw err.response === undefined
        ? ERRORS.errServer
        : err.response.data.data
    }
  }
} // Fim da classe

export default ActorApi
