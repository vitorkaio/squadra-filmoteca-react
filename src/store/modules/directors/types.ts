import { Action } from 'redux'
import { Director, DirectorAdd } from 'models/Director.model'

export enum DirectorTypes {
  DIRECTOR_REQUEST = 'DIRECTOR_REQUEST',
  DIRECTOR_SUCCCES = 'DIRECTOR_SUCCCES',
  DIRECTOR_ERROR = 'DIRECTOR_ERROR',
  DIRECTOR_RESET = 'DIRECTOR_RESET',

  DIRECTOR_ADD_REQUEST = 'DIRECTOR_ADD_REQUEST',
  DIRECTOR_ADD_SUCCCES = 'DIRECTOR_ADD_SUCCCES',
  DIRECTOR_ADD_ERROR = 'DIRECTOR_ADD_ERROR',
  DIRECTOR_ADD_RESET = 'DIRECTOR_ADD_RESET',

  DIRECTOR_REMOVE_REQUEST = 'DIRECTOR_REMOVE_REQUEST',
  DIRECTOR_REMOVE_SUCCCES = 'DIRECTOR_REMOVE_SUCCCES',
  DIRECTOR_REMOVE_ERROR = 'DIRECTOR_REMOVE_ERROR',
  DIRECTOR_REMOVE_RESET = 'DIRECTOR_REMOVE_RESET'
}

export interface DirectorState {
  directors: Director[] | undefined
  directorsLoading: boolean
  directorsError: boolean
  directorsErrorMsg: string

  directorAddSuccess: boolean
  directorAddLoading: boolean
  directorAddError: boolean
  directorAddErrorMsg: string

  directorRemoveSuccess: boolean
  directorRemoveLoading: boolean
  directorRemoveError: boolean
  directorRemoveErrorMsg: string
}

export interface DirectorAddAction extends Action {
  payload: {
    movieAdd: DirectorAdd
  }
}

export interface DirectorRemoveAction extends Action {
  payload: {
    id: number
  }
}

interface DirectorRequest {
  type: DirectorTypes.DIRECTOR_REQUEST
}

interface DirectorSucess {
  type: DirectorTypes.DIRECTOR_SUCCCES
  payload: {
    directors: Director[]
  }
}

interface DirectorError {
  type: DirectorTypes.DIRECTOR_ERROR
  payload: {
    msgError: string
  }
}

interface DirectorReset {
  type: DirectorTypes.DIRECTOR_RESET
}

// ************************************ ADD DIRECTOR ************************************

interface DirectorAddRequest {
  type: DirectorTypes.DIRECTOR_ADD_REQUEST
}

interface DirectorAddSucess {
  type: DirectorTypes.DIRECTOR_ADD_SUCCCES
  payload: {
    directors: Director[]
  }
}

interface DirectorAddError {
  type: DirectorTypes.DIRECTOR_ADD_ERROR
  payload: {
    msgError: string
  }
}

interface DirectorAddReset {
  type: DirectorTypes.DIRECTOR_ADD_RESET
}

// ************************************ Remove DIRECTOR ************************************

interface DirectorRemoveRequest {
  type: DirectorTypes.DIRECTOR_REMOVE_REQUEST
}

interface DirectorRemoveSucess {
  type: DirectorTypes.DIRECTOR_REMOVE_SUCCCES
  payload: {
    directors: Director[]
  }
}

interface DirectorRemoveError {
  type: DirectorTypes.DIRECTOR_REMOVE_ERROR
  payload: {
    msgError: string
  }
}

interface DirectorRemoveReset {
  type: DirectorTypes.DIRECTOR_REMOVE_RESET
}

export type DirectorActions = DirectorRequest &
  DirectorSucess &
  DirectorError &
  DirectorReset &
  DirectorAddRequest &
  DirectorAddSucess &
  DirectorAddError &
  DirectorAddReset &
  DirectorRemoveRequest &
  DirectorRemoveSucess &
  DirectorRemoveError &
  DirectorRemoveReset
