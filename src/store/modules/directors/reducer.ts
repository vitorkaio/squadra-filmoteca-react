import { Reducer } from 'redux'
import produce from 'immer'
import { DirectorState, DirectorTypes, DirectorActions } from './types'

const INITIAL_STATE: DirectorState = {
  directors: undefined,
  directorsLoading: false,
  directorsError: false,
  directorsErrorMsg: '',

  directorAddSuccess: false,
  directorAddLoading: false,
  directorAddError: false,
  directorAddErrorMsg: '',

  directorRemoveSuccess: false,
  directorRemoveLoading: false,
  directorRemoveError: false,
  directorRemoveErrorMsg: ''
}

const directorsReducer: Reducer<DirectorState, DirectorActions> = produce(
  (newDraft: DirectorState = INITIAL_STATE, action: DirectorActions) => {
    const draft = newDraft
    const { type, payload } = action
    switch (type) {
      // ********************* DIRECTORS *********************
      case DirectorTypes.DIRECTOR_REQUEST:
        draft.directorsLoading = true
        draft.directorsError = false
        draft.directorsErrorMsg = ''
        break

      case DirectorTypes.DIRECTOR_SUCCCES:
        draft.directors = payload.directors
        draft.directorsLoading = false
        draft.directorsError = false
        draft.directorsErrorMsg = ''
        break

      case DirectorTypes.DIRECTOR_ERROR:
        draft.directors = undefined
        draft.directorsLoading = false
        draft.directorsError = true
        draft.directorsErrorMsg = payload.msgError
        break

      case DirectorTypes.DIRECTOR_RESET:
        draft.directors = undefined
        draft.directorsLoading = false
        draft.directorsError = false
        draft.directorsErrorMsg = ''
        break

      // ************************************ ADD DIRECTOR ************************************
      case DirectorTypes.DIRECTOR_ADD_REQUEST:
        draft.directorAddLoading = true
        draft.directorAddError = false
        draft.directorAddErrorMsg = ''
        break

      case DirectorTypes.DIRECTOR_ADD_SUCCCES:
        draft.directors = payload.directors
        draft.directorAddSuccess = true
        draft.directorAddLoading = false
        draft.directorAddError = false
        draft.directorAddErrorMsg = ''
        break

      case DirectorTypes.DIRECTOR_ADD_ERROR:
        draft.directorAddSuccess = false
        draft.directorAddLoading = false
        draft.directorAddError = true
        draft.directorAddErrorMsg = payload.msgError
        break

      case DirectorTypes.DIRECTOR_ADD_RESET:
        draft.directorAddSuccess = false
        draft.directorAddLoading = false
        draft.directorAddError = false
        draft.directorAddErrorMsg = ''
        break

      // ************************************ Remove DIRECTOR ************************************
      case DirectorTypes.DIRECTOR_REMOVE_REQUEST:
        draft.directorRemoveLoading = true
        draft.directorRemoveError = false
        draft.directorRemoveErrorMsg = ''
        break

      case DirectorTypes.DIRECTOR_REMOVE_SUCCCES:
        draft.directors = payload.directors
        draft.directorRemoveSuccess = true
        draft.directorRemoveLoading = false
        draft.directorRemoveError = false
        draft.directorRemoveErrorMsg = ''
        break

      case DirectorTypes.DIRECTOR_REMOVE_ERROR:
        draft.directorRemoveSuccess = false
        draft.directorRemoveLoading = false
        draft.directorRemoveError = true
        draft.directorRemoveErrorMsg = payload.msgError
        break

      case DirectorTypes.DIRECTOR_REMOVE_RESET:
        draft.directorRemoveSuccess = false
        draft.directorRemoveLoading = false
        draft.directorRemoveError = false
        draft.directorRemoveErrorMsg = ''
        break

      default:
        return draft
    }
    return newDraft
  }
)

export default directorsReducer
