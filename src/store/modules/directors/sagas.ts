import { call, put, takeLatest } from 'redux-saga/effects'

import FormatErrors, { ERRORS } from 'services/FormatErrors'
import DirectorApi from 'services/DirectorApi'
import { Director } from 'models/Director.model'
import { DirectorTypes, DirectorAddAction, DirectorRemoveAction } from './types'
import {
  directorSuccess,
  directorError,
  directorAddSuccess,
  directorAddError,
  directorRemoveSuccess,
  directorRemoveError
} from './actions'

function* directorRequestLoad() {
  try {
    const directors: Director[] = yield call(DirectorApi.directors)
    yield put(directorSuccess(directors))
  } catch (err) {
    yield put(directorError(FormatErrors.format(err)))
  }
}

// ************************************ ADD DIRECTOR ************************************
function* directorAddRequestLoad(action: DirectorAddAction) {
  try {
    const { payload } = action
    const director: Director = yield call(
      DirectorApi.directorAdd,
      payload.movieAdd
    )
    if (director) {
      const directors: Director[] = yield call(DirectorApi.directors)
      if (directors) {
        yield put(directorAddSuccess(directors))
      } else {
        yield put(directorAddError(FormatErrors.format(ERRORS.errServer)))
      }
    } else {
      yield put(directorAddError(FormatErrors.format(ERRORS.errServer)))
    }
  } catch (err) {
    yield put(directorAddError(FormatErrors.format(err)))
  }
}

// ************************************ Remove DIRECTOR ************************************
function* directorRemoveRequestLoad(action: DirectorRemoveAction) {
  try {
    const { payload } = action
    const director: Director = yield call(
      DirectorApi.directorRemove,
      payload.id
    )
    if (director) {
      const directors: Director[] = yield call(DirectorApi.directors)
      if (directors) {
        yield put(directorRemoveSuccess(directors))
      } else {
        yield put(directorRemoveError(FormatErrors.format(ERRORS.errServer)))
      }
    } else {
      yield put(directorRemoveError(FormatErrors.format(ERRORS.errServer)))
    }
  } catch (err) {
    yield put(directorRemoveError(FormatErrors.format(err)))
  }
}

export default [
  takeLatest(DirectorTypes.DIRECTOR_REQUEST, directorRequestLoad),
  takeLatest(DirectorTypes.DIRECTOR_ADD_REQUEST, directorAddRequestLoad),
  takeLatest(DirectorTypes.DIRECTOR_REMOVE_REQUEST, directorRemoveRequestLoad)
]
