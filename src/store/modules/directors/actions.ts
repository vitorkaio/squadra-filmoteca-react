import { action } from 'typesafe-actions'

import { Director, DirectorAdd } from 'models/Director.model'
import { DirectorTypes } from './types'

// **** Director
export const directorRequest = () => action(DirectorTypes.DIRECTOR_REQUEST)
export const directorSuccess = (directors: Director[]) =>
  action(DirectorTypes.DIRECTOR_SUCCCES, { directors })
export const directorError = (msgError: string) =>
  action(DirectorTypes.DIRECTOR_ERROR, { msgError })
export const directorReset = () => action(DirectorTypes.DIRECTOR_RESET)

// ************************************ ADD DIRECTOR ************************************

// **** Director Add
export const directorAddRequest = (movieAdd: DirectorAdd) =>
  action(DirectorTypes.DIRECTOR_ADD_REQUEST, { movieAdd })
export const directorAddSuccess = (directors: Director[]) =>
  action(DirectorTypes.DIRECTOR_ADD_SUCCCES, { directors })
export const directorAddError = (msgError: string) =>
  action(DirectorTypes.DIRECTOR_ADD_ERROR, { msgError })
export const directorAddReset = () => action(DirectorTypes.DIRECTOR_ADD_RESET)

// ************************************ Remove DIRECTOR ************************************

// **** Director Remove
export const directorRemoveRequest = (id: number) =>
  action(DirectorTypes.DIRECTOR_REMOVE_REQUEST, { id })
export const directorRemoveSuccess = (directors: Director[]) =>
  action(DirectorTypes.DIRECTOR_REMOVE_SUCCCES, { directors })
export const directorRemoveError = (msgError: string) =>
  action(DirectorTypes.DIRECTOR_REMOVE_ERROR, { msgError })
export const directorRemoveReset = () =>
  action(DirectorTypes.DIRECTOR_REMOVE_RESET)
