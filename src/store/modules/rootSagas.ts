import { all } from 'redux-saga/effects'
import movieSagas from './movies/sagas'
import actorSagas from './actors/sagas'
import directorSagas from './directors/sagas'

export default function* rootSaga() {
  return yield all([...movieSagas, ...actorSagas, ...directorSagas])
}
