import { MovieActions } from './movies/types'
import { ActorActions } from './actors/types'
import { DirectorActions } from './directors/types'

export type rootActions = MovieActions | ActorActions | DirectorActions
