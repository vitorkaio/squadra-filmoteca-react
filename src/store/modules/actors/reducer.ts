import { Reducer } from 'redux'
import produce from 'immer'
import { ActorState, ActorTypes, ActorActions } from './types'

const INITIAL_STATE: ActorState = {
  actors: undefined,
  actorsLoading: false,
  actorsError: false,
  actorsErrorMsg: '',

  actorAddSuccess: false,
  actorAddLoading: false,
  actorAddError: false,
  actorAddErrorMsg: '',

  actorRemoveSuccess: false,
  actorRemoveLoading: false,
  actorRemoveError: false,
  actorRemoveErrorMsg: ''
}

const actorsReducer: Reducer<ActorState, ActorActions> = produce(
  (newDraft: ActorState = INITIAL_STATE, action: ActorActions) => {
    const draft = newDraft
    const { type, payload } = action
    switch (type) {
      // ********************* ACTORS *********************
      case ActorTypes.ACTOR_REQUEST:
        draft.actorsLoading = true
        draft.actorsError = false
        draft.actorsErrorMsg = ''
        break

      case ActorTypes.ACTOR_SUCCCES:
        draft.actors = payload.actors
        draft.actorsLoading = false
        draft.actorsError = false
        draft.actorsErrorMsg = ''
        break

      case ActorTypes.ACTOR_ERROR:
        draft.actors = undefined
        draft.actorsLoading = false
        draft.actorsError = true
        draft.actorsErrorMsg = payload.msgError
        break

      case ActorTypes.ACTOR_RESET:
        draft.actors = undefined
        draft.actorsLoading = false
        draft.actorsError = false
        draft.actorsErrorMsg = ''
        break

      // ************************************ ADD ACTOR ************************************
      case ActorTypes.ACTOR_ADD_REQUEST:
        draft.actorAddLoading = true
        draft.actorAddError = false
        draft.actorAddErrorMsg = ''
        break

      case ActorTypes.ACTOR_ADD_SUCCCES:
        draft.actors = payload.actors
        draft.actorAddSuccess = true
        draft.actorAddLoading = false
        draft.actorAddError = false
        draft.actorAddErrorMsg = ''
        break

      case ActorTypes.ACTOR_ADD_ERROR:
        draft.actorAddSuccess = false
        draft.actorAddLoading = false
        draft.actorAddError = true
        draft.actorAddErrorMsg = payload.msgError
        break

      case ActorTypes.ACTOR_ADD_RESET:
        draft.actorAddSuccess = false
        draft.actorAddLoading = false
        draft.actorAddError = false
        draft.actorAddErrorMsg = ''
        break

      // ************************************ Remove ACTOR ************************************
      case ActorTypes.ACTOR_REMOVE_REQUEST:
        draft.actorRemoveLoading = true
        draft.actorRemoveError = false
        draft.actorRemoveErrorMsg = ''
        break

      case ActorTypes.ACTOR_REMOVE_SUCCCES:
        draft.actors = payload.actors
        draft.actorRemoveSuccess = true
        draft.actorRemoveLoading = false
        draft.actorRemoveError = false
        draft.actorRemoveErrorMsg = ''
        break

      case ActorTypes.ACTOR_REMOVE_ERROR:
        draft.actorRemoveSuccess = false
        draft.actorRemoveLoading = false
        draft.actorRemoveError = true
        draft.actorRemoveErrorMsg = payload.msgError
        break

      case ActorTypes.ACTOR_REMOVE_RESET:
        draft.actorRemoveSuccess = false
        draft.actorRemoveLoading = false
        draft.actorRemoveError = false
        draft.actorRemoveErrorMsg = ''
        break

      default:
        return draft
    }
    return newDraft
  }
)

export default actorsReducer
