import { call, put, takeLatest } from 'redux-saga/effects'

import FormatErrors, { ERRORS } from 'services/FormatErrors'
import ActorApi from 'services/ActorsApi'
import { Actor } from 'models/Actor.model'
import { ActorTypes, ActorAddAction, ActorRemoveAction } from './types'
import {
  actorSuccess,
  actorError,
  actorAddSuccess,
  actorAddError,
  actorRemoveSuccess,
  actorRemoveError
} from './actions'

function* actorRequestLoad() {
  try {
    const actors: Actor[] = yield call(ActorApi.actors)
    yield put(actorSuccess(actors))
  } catch (err) {
    yield put(actorError(FormatErrors.format(err)))
  }
}

// ************************************ ADD ACTOR ************************************
function* actorAddRequestLoad(action: ActorAddAction) {
  try {
    const { payload } = action
    const actor: Actor = yield call(ActorApi.actorAdd, payload.movieAdd)
    if (actor) {
      const actors: Actor[] = yield call(ActorApi.actors)
      if (actors) {
        yield put(actorAddSuccess(actors))
      } else {
        yield put(actorAddError(FormatErrors.format(ERRORS.errServer)))
      }
    } else {
      yield put(actorAddError(FormatErrors.format(ERRORS.errServer)))
    }
  } catch (err) {
    yield put(actorAddError(FormatErrors.format(err)))
  }
}

// ************************************ Remove ACTOR ************************************
function* actorRemoveRequestLoad(action: ActorRemoveAction) {
  try {
    const { payload } = action
    const actor: Actor = yield call(ActorApi.actorRemove, payload.id)
    if (actor) {
      const actors: Actor[] = yield call(ActorApi.actors)
      if (actors) {
        yield put(actorRemoveSuccess(actors))
      } else {
        yield put(actorRemoveError(FormatErrors.format(ERRORS.errServer)))
      }
    } else {
      yield put(actorRemoveError(FormatErrors.format(ERRORS.errServer)))
    }
  } catch (err) {
    yield put(actorRemoveError(FormatErrors.format(err)))
  }
}

export default [
  takeLatest(ActorTypes.ACTOR_REQUEST, actorRequestLoad),
  takeLatest(ActorTypes.ACTOR_ADD_REQUEST, actorAddRequestLoad),
  takeLatest(ActorTypes.ACTOR_REMOVE_REQUEST, actorRemoveRequestLoad)
]
