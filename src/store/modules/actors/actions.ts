import { action } from 'typesafe-actions'

import { Actor, ActorAdd } from 'models/Actor.model'
import { ActorTypes } from './types'

// **** Actor
export const actorRequest = () => action(ActorTypes.ACTOR_REQUEST)
export const actorSuccess = (actors: Actor[]) =>
  action(ActorTypes.ACTOR_SUCCCES, { actors })
export const actorError = (msgError: string) =>
  action(ActorTypes.ACTOR_ERROR, { msgError })
export const actorReset = () => action(ActorTypes.ACTOR_RESET)

// ************************************ ADD ACTOR ************************************

// **** Actor Add
export const actorAddRequest = (movieAdd: ActorAdd) =>
  action(ActorTypes.ACTOR_ADD_REQUEST, { movieAdd })
export const actorAddSuccess = (actors: Actor[]) =>
  action(ActorTypes.ACTOR_ADD_SUCCCES, { actors })
export const actorAddError = (msgError: string) =>
  action(ActorTypes.ACTOR_ADD_ERROR, { msgError })
export const actorAddReset = () => action(ActorTypes.ACTOR_ADD_RESET)

// ************************************ Remove ACTOR ************************************

// **** Actor Remove
export const actorRemoveRequest = (id: number) =>
  action(ActorTypes.ACTOR_REMOVE_REQUEST, { id })
export const actorRemoveSuccess = (actors: Actor[]) =>
  action(ActorTypes.ACTOR_REMOVE_SUCCCES, { actors })
export const actorRemoveError = (msgError: string) =>
  action(ActorTypes.ACTOR_REMOVE_ERROR, { msgError })
export const actorRemoveReset = () => action(ActorTypes.ACTOR_REMOVE_RESET)
