import { Action } from 'redux'
import { Actor, ActorAdd } from 'models/Actor.model'

export enum ActorTypes {
  ACTOR_REQUEST = 'ACTOR_REQUEST',
  ACTOR_SUCCCES = 'ACTOR_SUCCCES',
  ACTOR_ERROR = 'ACTOR_ERROR',
  ACTOR_RESET = 'ACTOR_RESET',

  ACTOR_ADD_REQUEST = 'ACTOR_ADD_REQUEST',
  ACTOR_ADD_SUCCCES = 'ACTOR_ADD_SUCCCES',
  ACTOR_ADD_ERROR = 'ACTOR_ADD_ERROR',
  ACTOR_ADD_RESET = 'ACTOR_ADD_RESET',

  ACTOR_REMOVE_REQUEST = 'ACTOR_REMOVE_REQUEST',
  ACTOR_REMOVE_SUCCCES = 'ACTOR_REMOVE_SUCCCES',
  ACTOR_REMOVE_ERROR = 'ACTOR_REMOVE_ERROR',
  ACTOR_REMOVE_RESET = 'ACTOR_REMOVE_RESET'
}

export interface ActorState {
  actors: Actor[] | undefined
  actorsLoading: boolean
  actorsError: boolean
  actorsErrorMsg: string

  actorAddSuccess: boolean
  actorAddLoading: boolean
  actorAddError: boolean
  actorAddErrorMsg: string

  actorRemoveSuccess: boolean
  actorRemoveLoading: boolean
  actorRemoveError: boolean
  actorRemoveErrorMsg: string
}

export interface ActorAddAction extends Action {
  payload: {
    movieAdd: ActorAdd
  }
}

export interface ActorRemoveAction extends Action {
  payload: {
    id: number
  }
}

interface ActorRequest {
  type: ActorTypes.ACTOR_REQUEST
}

interface ActorSucess {
  type: ActorTypes.ACTOR_SUCCCES
  payload: {
    actors: Actor[]
  }
}

interface ActorError {
  type: ActorTypes.ACTOR_ERROR
  payload: {
    msgError: string
  }
}

interface ActorReset {
  type: ActorTypes.ACTOR_RESET
}

// ************************************ ADD ACTOR ************************************

interface ActorAddRequest {
  type: ActorTypes.ACTOR_ADD_REQUEST
}

interface ActorAddSucess {
  type: ActorTypes.ACTOR_ADD_SUCCCES
  payload: {
    actors: Actor[]
  }
}

interface ActorAddError {
  type: ActorTypes.ACTOR_ADD_ERROR
  payload: {
    msgError: string
  }
}

interface ActorAddReset {
  type: ActorTypes.ACTOR_ADD_RESET
}

// ************************************ Remove ACTOR ************************************

interface ActorRemoveRequest {
  type: ActorTypes.ACTOR_REMOVE_REQUEST
}

interface ActorRemoveSucess {
  type: ActorTypes.ACTOR_REMOVE_SUCCCES
  payload: {
    actors: Actor[]
  }
}

interface ActorRemoveError {
  type: ActorTypes.ACTOR_REMOVE_ERROR
  payload: {
    msgError: string
  }
}

interface ActorRemoveReset {
  type: ActorTypes.ACTOR_REMOVE_RESET
}

export type ActorActions = ActorRequest &
  ActorSucess &
  ActorError &
  ActorReset &
  ActorAddRequest &
  ActorAddSucess &
  ActorAddError &
  ActorAddReset &
  ActorRemoveRequest &
  ActorRemoveSucess &
  ActorRemoveError &
  ActorRemoveReset
