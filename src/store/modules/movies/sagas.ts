import { call, put, takeLatest } from 'redux-saga/effects'

import FormatErrors, { ERRORS } from 'services/FormatErrors'
import MovieApi from 'services/MoviesApi'
import { Movie } from 'models/Movie.model'
import {
  MovieTypes,
  MovieAddAction,
  MovieRemoveAction,
  MovieEditAction,
  ManagerActorMovieAction,
  ManagerDirectorMovieAction
} from './types'
import {
  movieSuccess,
  movieError,
  movieAddSuccess,
  movieAddError,
  movieRemoveSuccess,
  movieRemoveError,
  movieEditSuccess,
  movieEditError,
  deleteActorMovieSuccess,
  deleteActorMovieError,
  addActorMovieSuccess,
  addActorMovieError,
  deleteDirectorMovieSuccess,
  deleteDirectorMovieError,
  addDirectorMovieSuccess,
  addDirectorMovieError
} from './actions'

function* movieRequestLoad() {
  try {
    const movies: Movie[] = yield call(MovieApi.movies)
    yield put(movieSuccess(movies))
  } catch (err) {
    yield put(movieError(FormatErrors.format(err)))
  }
}

// ************************************ ADD MOVIE ************************************
function* movieAddRequestLoad(action: MovieAddAction) {
  try {
    const { payload } = action
    const movie: Movie = yield call(MovieApi.movieAdd, payload.movieAdd)
    if (movie) {
      const movies: Movie[] = yield call(MovieApi.movies)
      if (movies) {
        yield put(movieAddSuccess(movies))
      } else {
        yield put(movieAddError(FormatErrors.format(ERRORS.errServer)))
      }
    } else {
      yield put(movieAddError(FormatErrors.format(ERRORS.errServer)))
    }
  } catch (err) {
    yield put(movieAddError(FormatErrors.format(err)))
  }
}

// ************************************ Remove MOVIE ************************************
function* movieRemoveRequestLoad(action: MovieRemoveAction) {
  try {
    const { payload } = action
    const movie: Movie = yield call(MovieApi.movieRemove, payload.id)
    if (movie) {
      const movies: Movie[] = yield call(MovieApi.movies)
      if (movies) {
        yield put(movieRemoveSuccess(movies))
      } else {
        yield put(movieRemoveError(FormatErrors.format(ERRORS.errServer)))
      }
    } else {
      yield put(movieRemoveError(FormatErrors.format(ERRORS.errServer)))
    }
  } catch (err) {
    yield put(movieRemoveError(FormatErrors.format(err)))
  }
}

// ************************************ Edit MOVIE ************************************

function* movieEditRequestLoad(action: MovieEditAction) {
  try {
    const { payload } = action
    const movie: Movie = yield call(MovieApi.movieEdit, payload.movieEdit)
    if (movie) {
      const movies: Movie[] = yield call(MovieApi.movies)
      if (movies) {
        yield put(movieEditSuccess(movies))
      } else {
        yield put(movieEditError(FormatErrors.format(ERRORS.errServer)))
      }
    } else {
      yield put(movieEditError(FormatErrors.format(ERRORS.errServer)))
    }
  } catch (err) {
    yield put(movieEditError(FormatErrors.format(err)))
  }
}

// ************************************ Delete Actor Of Movie ************************************
function* deleteActorMovieRequestLoad(action: ManagerActorMovieAction) {
  try {
    const { payload } = action
    const movie: Movie = yield call(
      MovieApi.deleteActorMovie,
      payload.managerActorMovie
    )
    if (movie) {
      const movies: Movie[] = yield call(MovieApi.movies)
      if (movies) {
        yield put(deleteActorMovieSuccess(movies))
      } else {
        yield put(deleteActorMovieError(FormatErrors.format(ERRORS.errServer)))
      }
    } else {
      yield put(deleteActorMovieError(FormatErrors.format(ERRORS.errServer)))
    }
  } catch (err) {
    yield put(deleteActorMovieError(FormatErrors.format(err)))
  }
}

// ************************************ Add Actor Of Movie ************************************
function* addActorMovieRequestLoad(action: ManagerActorMovieAction) {
  try {
    const { payload } = action
    const movie: Movie = yield call(
      MovieApi.addActorMovie,
      payload.managerActorMovie
    )
    if (movie) {
      const movies: Movie[] = yield call(MovieApi.movies)
      if (movies) {
        yield put(addActorMovieSuccess(movies))
      } else {
        yield put(addActorMovieError(FormatErrors.format(ERRORS.errServer)))
      }
    } else {
      yield put(addActorMovieError(FormatErrors.format(ERRORS.errServer)))
    }
  } catch (err) {
    yield put(addActorMovieError(FormatErrors.format(err)))
  }
}

// ************************************ Delete Director Of Movie ************************************
function* deleteDirectorMovieRequestLoad(action: ManagerDirectorMovieAction) {
  try {
    const { payload } = action
    const movie: Movie = yield call(
      MovieApi.deleteDirectorMovie,
      payload.managerDirectorMovie
    )
    if (movie) {
      const movies: Movie[] = yield call(MovieApi.movies)
      if (movies) {
        yield put(deleteDirectorMovieSuccess(movies))
      } else {
        yield put(
          deleteDirectorMovieError(FormatErrors.format(ERRORS.errServer))
        )
      }
    } else {
      yield put(deleteDirectorMovieError(FormatErrors.format(ERRORS.errServer)))
    }
  } catch (err) {
    yield put(deleteDirectorMovieError(FormatErrors.format(err)))
  }
}

// ************************************ Add Director Of Movie ************************************
function* addDirectorMovieRequestLoad(action: ManagerDirectorMovieAction) {
  try {
    const { payload } = action
    const movie: Movie = yield call(
      MovieApi.addDirectorMovie,
      payload.managerDirectorMovie
    )
    if (movie) {
      const movies: Movie[] = yield call(MovieApi.movies)
      if (movies) {
        yield put(addDirectorMovieSuccess(movies))
      } else {
        yield put(addDirectorMovieError(FormatErrors.format(ERRORS.errServer)))
      }
    } else {
      yield put(addDirectorMovieError(FormatErrors.format(ERRORS.errServer)))
    }
  } catch (err) {
    yield put(addDirectorMovieError(FormatErrors.format(err)))
  }
}

export default [
  takeLatest(MovieTypes.MOVIE_REQUEST, movieRequestLoad),
  takeLatest(MovieTypes.MOVIE_ADD_REQUEST, movieAddRequestLoad),
  takeLatest(MovieTypes.MOVIE_REMOVE_REQUEST, movieRemoveRequestLoad),
  takeLatest(MovieTypes.MOVIE_EDIT_REQUEST, movieEditRequestLoad),
  takeLatest(
    MovieTypes.DELETE_ACTOR_MOVIE_REQUEST,
    deleteActorMovieRequestLoad
  ),
  takeLatest(MovieTypes.ADD_ACTOR_MOVIE_REQUEST, addActorMovieRequestLoad),
  takeLatest(
    MovieTypes.DELETE_DIRECTOR_MOVIE_REQUEST,
    deleteDirectorMovieRequestLoad
  ),
  takeLatest(MovieTypes.ADD_DIRECTOR_MOVIE_REQUEST, addDirectorMovieRequestLoad)
]
