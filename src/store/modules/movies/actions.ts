import { action } from 'typesafe-actions'

import {
  Movie,
  MovieAdd,
  MovieEdit,
  ManagerActorMovie,
  ManagerDirectorMovie
} from 'models/Movie.model'
import { MovieTypes } from './types'

// **** Movie
export const movieRequest = () => action(MovieTypes.MOVIE_REQUEST)
export const movieSuccess = (movies: Movie[]) =>
  action(MovieTypes.MOVIE_SUCCCES, { movies })
export const movieError = (msgError: string) =>
  action(MovieTypes.MOVIE_ERROR, { msgError })
export const movieReset = () => action(MovieTypes.MOVIE_RESET)

// ************************************ ADD MOVIE ************************************

export const movieAddRequest = (movieAdd: MovieAdd) =>
  action(MovieTypes.MOVIE_ADD_REQUEST, { movieAdd })
export const movieAddSuccess = (movies: Movie[]) =>
  action(MovieTypes.MOVIE_ADD_SUCCCES, { movies })
export const movieAddError = (msgError: string) =>
  action(MovieTypes.MOVIE_ADD_ERROR, { msgError })
export const movieAddReset = () => action(MovieTypes.MOVIE_ADD_RESET)

// ************************************ Remove MOVIE ************************************

export const movieRemoveRequest = (id: number) =>
  action(MovieTypes.MOVIE_REMOVE_REQUEST, { id })
export const movieRemoveSuccess = (movies: Movie[]) =>
  action(MovieTypes.MOVIE_REMOVE_SUCCCES, { movies })
export const movieRemoveError = (msgError: string) =>
  action(MovieTypes.MOVIE_REMOVE_ERROR, { msgError })
export const movieRemoveReset = () => action(MovieTypes.MOVIE_REMOVE_RESET)

// ************************************ Edit MOVIE ************************************

export const movieEditRequest = (movieEdit: MovieEdit) =>
  action(MovieTypes.MOVIE_EDIT_REQUEST, { movieEdit })
export const movieEditSuccess = (movies: Movie[]) =>
  action(MovieTypes.MOVIE_EDIT_SUCCCES, { movies })
export const movieEditError = (msgError: string) =>
  action(MovieTypes.MOVIE_EDIT_ERROR, { msgError })
export const movieEditReset = () => action(MovieTypes.MOVIE_EDIT_RESET)

// ************************************ Delete Actor Of Movie ************************************

export const deleteActorMovieRequest = (managerActorMovie: ManagerActorMovie) =>
  action(MovieTypes.DELETE_ACTOR_MOVIE_REQUEST, { managerActorMovie })
export const deleteActorMovieSuccess = (movies: Movie[]) =>
  action(MovieTypes.DELETE_ACTOR_MOVIE_SUCCESS, { movies })
export const deleteActorMovieError = (msgError: string) =>
  action(MovieTypes.DELETE_ACTOR_MOVIE_ERROR, { msgError })
export const deleteActorMovieReset = () =>
  action(MovieTypes.DELETE_ACTOR_MOVIE_RESET)

// ************************************ Add Actor Of Movie ************************************

export const addActorMovieRequest = (managerActorMovie: ManagerActorMovie) =>
  action(MovieTypes.ADD_ACTOR_MOVIE_REQUEST, { managerActorMovie })
export const addActorMovieSuccess = (movies: Movie[]) =>
  action(MovieTypes.ADD_ACTOR_MOVIE_SUCCESS, { movies })
export const addActorMovieError = (msgError: string) =>
  action(MovieTypes.ADD_ACTOR_MOVIE_ERROR, { msgError })
export const addActorMovieReset = () => action(MovieTypes.ADD_ACTOR_MOVIE_RESET)

// ************************************ Delete Director Of Movie ************************************

export const deleteDirectorMovieRequest = (
  managerDirectorMovie: ManagerDirectorMovie
) => action(MovieTypes.DELETE_DIRECTOR_MOVIE_REQUEST, { managerDirectorMovie })
export const deleteDirectorMovieSuccess = (movies: Movie[]) =>
  action(MovieTypes.DELETE_DIRECTOR_MOVIE_SUCCESS, { movies })
export const deleteDirectorMovieError = (msgError: string) =>
  action(MovieTypes.DELETE_DIRECTOR_MOVIE_ERROR, { msgError })
export const deleteDirectorMovieReset = () =>
  action(MovieTypes.DELETE_DIRECTOR_MOVIE_RESET)

// ************************************ Add Director Of Movie ************************************

export const addDirectorMovieRequest = (
  managerDirectorMovie: ManagerDirectorMovie
) => action(MovieTypes.ADD_DIRECTOR_MOVIE_REQUEST, { managerDirectorMovie })
export const addDirectorMovieSuccess = (movies: Movie[]) =>
  action(MovieTypes.ADD_DIRECTOR_MOVIE_SUCCESS, { movies })
export const addDirectorMovieError = (msgError: string) =>
  action(MovieTypes.ADD_DIRECTOR_MOVIE_ERROR, { msgError })
export const addDirectorMovieReset = () =>
  action(MovieTypes.ADD_DIRECTOR_MOVIE_RESET)
