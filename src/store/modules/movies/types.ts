import { Action } from 'redux'
import {
  Movie,
  MovieAdd,
  MovieEdit,
  ManagerActorMovie,
  ManagerDirectorMovie
} from 'models/Movie.model'

export enum MovieTypes {
  MOVIE_REQUEST = 'MOVIE_REQUEST',
  MOVIE_SUCCCES = 'MOVIE_SUCCCES',
  MOVIE_ERROR = 'MOVIE_ERROR',
  MOVIE_RESET = 'MOVIE_RESET',

  MOVIE_ADD_REQUEST = 'MOVIE_ADD_REQUEST',
  MOVIE_ADD_SUCCCES = 'MOVIE_ADD_SUCCCES',
  MOVIE_ADD_ERROR = 'MOVIE_ADD_ERROR',
  MOVIE_ADD_RESET = 'MOVIE_ADD_RESET',

  MOVIE_REMOVE_REQUEST = 'MOVIE_REMOVE_REQUEST',
  MOVIE_REMOVE_SUCCCES = 'MOVIE_REMOVE_SUCCCES',
  MOVIE_REMOVE_ERROR = 'MOVIE_REMOVE_ERROR',
  MOVIE_REMOVE_RESET = 'MOVIE_REMOVE_RESET',

  MOVIE_EDIT_REQUEST = 'MOVIE_EDIT_REQUEST',
  MOVIE_EDIT_SUCCCES = 'MOVIE_EDIT_SUCCCES',
  MOVIE_EDIT_ERROR = 'MOVIE_EDIT_ERROR',
  MOVIE_EDIT_RESET = 'MOVIE_EDIT_RESET',

  DELETE_ACTOR_MOVIE_REQUEST = 'DELETE_ACTOR_MOVIE_REQUEST',
  DELETE_ACTOR_MOVIE_SUCCESS = 'DELETE_ACTOR_MOVIE_SUCCESS',
  DELETE_ACTOR_MOVIE_ERROR = 'DELETE_ACTOR_MOVIE_ERROR',
  DELETE_ACTOR_MOVIE_RESET = 'DELETE_ACTOR_MOVIE_RESET',

  ADD_ACTOR_MOVIE_REQUEST = 'ADD_ACTOR_MOVIE_REQUEST',
  ADD_ACTOR_MOVIE_SUCCESS = 'ADD_ACTOR_MOVIE_SUCCESS',
  ADD_ACTOR_MOVIE_ERROR = 'ADD_ACTOR_MOVIE_ERROR',
  ADD_ACTOR_MOVIE_RESET = 'ADD_ACTOR_MOVIE_RESET',

  DELETE_DIRECTOR_MOVIE_REQUEST = 'DELETE_DIRECTOR_MOVIE_REQUEST',
  DELETE_DIRECTOR_MOVIE_SUCCESS = 'DELETE_DIRECTOR_MOVIE_SUCCESS',
  DELETE_DIRECTOR_MOVIE_ERROR = 'DELETE_DIRECTOR_MOVIE_ERROR',
  DELETE_DIRECTOR_MOVIE_RESET = 'DELETE_DIRECTOR_MOVIE_RESET',

  ADD_DIRECTOR_MOVIE_REQUEST = 'ADD_DIRECTOR_MOVIE_REQUEST',
  ADD_DIRECTOR_MOVIE_SUCCESS = 'ADD_DIRECTOR_MOVIE_SUCCESS',
  ADD_DIRECTOR_MOVIE_ERROR = 'ADD_DIRECTOR_MOVIE_ERROR',
  ADD_DIRECTOR_MOVIE_RESET = 'ADD_DIRECTOR_MOVIE_RESET'
}

export interface MovieState {
  movies: Movie[] | undefined
  moviesLoading: boolean
  moviesError: boolean
  moviesErrorMsg: string

  movieAddSuccess: boolean
  movieAddLoading: boolean
  movieAddError: boolean
  movieAddErrorMsg: string

  movieRemoveSuccess: boolean
  movieRemoveLoading: boolean
  movieRemoveError: boolean
  movieRemoveErrorMsg: string

  movieEditSuccess: boolean
  movieEditLoading: boolean
  movieEditError: boolean
  movieEditErrorMsg: string

  deleteActorMovieSuccess: boolean
  deleteActorMovieLoading: boolean
  deleteActorMovieError: boolean
  deleteActorMovieErrorMsg: string

  addActorMovieSuccess: boolean
  addActorMovieLoading: boolean
  addActorMovieError: boolean
  addActorMovieErrorMsg: string

  deleteDirectorMovieSuccess: boolean
  deleteDirectorMovieLoading: boolean
  deleteDirectorMovieError: boolean
  deleteDirectorMovieErrorMsg: string

  addDirectorMovieSuccess: boolean
  addDirectorMovieLoading: boolean
  addDirectorMovieError: boolean
  addDirectorMovieErrorMsg: string
}

export interface MovieAddAction extends Action {
  payload: {
    movieAdd: MovieAdd
  }
}

export interface MovieRemoveAction extends Action {
  payload: {
    id: number
  }
}

export interface MovieEditAction extends Action {
  payload: {
    movieEdit: MovieEdit
  }
}

export interface ManagerActorMovieAction extends Action {
  payload: {
    managerActorMovie: ManagerActorMovie
  }
}

export interface ManagerDirectorMovieAction extends Action {
  payload: {
    managerDirectorMovie: ManagerDirectorMovie
  }
}

interface MovieRequest {
  type: MovieTypes.MOVIE_REQUEST
}

interface MovieSucess {
  type: MovieTypes.MOVIE_SUCCCES
  payload: {
    movies: Movie[]
  }
}

interface MovieError {
  type: MovieTypes.MOVIE_ERROR
  payload: {
    msgError: string
  }
}

interface MovieReset {
  type: MovieTypes.MOVIE_RESET
}

// ************************************ ADD MOVIE ************************************

interface MovieAddRequest {
  type: MovieTypes.MOVIE_ADD_REQUEST
}

interface MovieAddSucess {
  type: MovieTypes.MOVIE_ADD_SUCCCES
  payload: {
    movies: Movie[]
  }
}

interface MovieAddError {
  type: MovieTypes.MOVIE_ADD_ERROR
  payload: {
    msgError: string
  }
}

interface MovieAddReset {
  type: MovieTypes.MOVIE_ADD_RESET
}

// ************************************ Remove MOVIE ************************************

interface MovieRemoveRequest {
  type: MovieTypes.MOVIE_REMOVE_REQUEST
}

interface MovieRemoveSucess {
  type: MovieTypes.MOVIE_REMOVE_SUCCCES
  payload: {
    movies: Movie[]
  }
}

interface MovieRemoveError {
  type: MovieTypes.MOVIE_REMOVE_ERROR
  payload: {
    msgError: string
  }
}

interface MovieRemoveReset {
  type: MovieTypes.MOVIE_REMOVE_RESET
}

// ************************************ Edit MOVIE ************************************
interface MovieEditRequest {
  type: MovieTypes.MOVIE_EDIT_REQUEST
}

interface MovieEditSucess {
  type: MovieTypes.MOVIE_EDIT_SUCCCES
  payload: {
    movies: Movie[]
  }
}

interface MovieEditError {
  type: MovieTypes.MOVIE_EDIT_ERROR
  payload: {
    msgError: string
  }
}

interface MovieEditReset {
  type: MovieTypes.MOVIE_EDIT_RESET
}

// ************************************ Delete Actor Of Movie ************************************

interface DeleteActorMovieRequest {
  type: MovieTypes.DELETE_ACTOR_MOVIE_REQUEST
}

interface DeleteActorMovieSucess {
  type: MovieTypes.DELETE_ACTOR_MOVIE_SUCCESS
  payload: {
    movies: Movie[]
  }
}

interface DeleteActorMovieError {
  type: MovieTypes.DELETE_ACTOR_MOVIE_ERROR
  payload: {
    msgError: string
  }
}

interface DeleteActorMovieReset {
  type: MovieTypes.DELETE_ACTOR_MOVIE_RESET
}

// ************************************ Add Actor Of Movie ************************************

interface AddActorMovieRequest {
  type: MovieTypes.ADD_ACTOR_MOVIE_REQUEST
}

interface AddActorMovieSucess {
  type: MovieTypes.ADD_ACTOR_MOVIE_SUCCESS
  payload: {
    movies: Movie[]
  }
}

interface AddActorMovieError {
  type: MovieTypes.ADD_ACTOR_MOVIE_ERROR
  payload: {
    msgError: string
  }
}

interface AddActorMovieReset {
  type: MovieTypes.ADD_ACTOR_MOVIE_RESET
}

// ************************************ Delete Director Of Movie ************************************

interface DeleteDirectorMovieRequest {
  type: MovieTypes.DELETE_DIRECTOR_MOVIE_REQUEST
}

interface DeleteDirectorMovieSucess {
  type: MovieTypes.DELETE_DIRECTOR_MOVIE_SUCCESS
  payload: {
    movies: Movie[]
  }
}

interface DeleteDirectorMovieError {
  type: MovieTypes.DELETE_DIRECTOR_MOVIE_ERROR
  payload: {
    msgError: string
  }
}

interface DeleteDirectorMovieReset {
  type: MovieTypes.DELETE_DIRECTOR_MOVIE_RESET
}

// ************************************ Add Director Of Movie ************************************

interface AddDirectorMovieRequest {
  type: MovieTypes.ADD_DIRECTOR_MOVIE_REQUEST
}

interface AddDirectorMovieSucess {
  type: MovieTypes.ADD_DIRECTOR_MOVIE_SUCCESS
  payload: {
    movies: Movie[]
  }
}

interface AddDirectorMovieError {
  type: MovieTypes.ADD_DIRECTOR_MOVIE_ERROR
  payload: {
    msgError: string
  }
}

interface AddDirectorMovieReset {
  type: MovieTypes.ADD_DIRECTOR_MOVIE_RESET
}

export type MovieActions = MovieRequest &
  MovieSucess &
  MovieError &
  MovieReset &
  MovieAddRequest &
  MovieAddSucess &
  MovieAddError &
  MovieAddReset &
  MovieRemoveRequest &
  MovieRemoveSucess &
  MovieRemoveError &
  MovieRemoveReset &
  MovieEditRequest &
  MovieEditSucess &
  MovieEditError &
  MovieEditReset &
  DeleteActorMovieRequest &
  DeleteActorMovieSucess &
  DeleteActorMovieError &
  DeleteActorMovieReset &
  AddActorMovieRequest &
  AddActorMovieSucess &
  AddActorMovieError &
  AddActorMovieReset &
  DeleteDirectorMovieRequest &
  DeleteDirectorMovieSucess &
  DeleteDirectorMovieError &
  DeleteDirectorMovieReset &
  AddDirectorMovieRequest &
  AddDirectorMovieSucess &
  AddDirectorMovieError &
  AddDirectorMovieReset
