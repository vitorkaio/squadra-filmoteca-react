import { Reducer } from 'redux'
import produce from 'immer'
import { MovieState, MovieTypes, MovieActions } from './types'

const INITIAL_STATE: MovieState = {
  movies: undefined,
  moviesLoading: false,
  moviesError: false,
  moviesErrorMsg: '',

  movieAddSuccess: false,
  movieAddLoading: false,
  movieAddError: false,
  movieAddErrorMsg: '',

  movieRemoveSuccess: false,
  movieRemoveLoading: false,
  movieRemoveError: false,
  movieRemoveErrorMsg: '',

  movieEditSuccess: false,
  movieEditLoading: false,
  movieEditError: false,
  movieEditErrorMsg: '',

  deleteActorMovieSuccess: false,
  deleteActorMovieLoading: false,
  deleteActorMovieError: false,
  deleteActorMovieErrorMsg: '',

  addActorMovieSuccess: false,
  addActorMovieLoading: false,
  addActorMovieError: false,
  addActorMovieErrorMsg: '',

  deleteDirectorMovieSuccess: false,
  deleteDirectorMovieLoading: false,
  deleteDirectorMovieError: false,
  deleteDirectorMovieErrorMsg: '',

  addDirectorMovieSuccess: false,
  addDirectorMovieLoading: false,
  addDirectorMovieError: false,
  addDirectorMovieErrorMsg: ''
}

const moviesReducer: Reducer<MovieState, MovieActions> = produce(
  (newDraft: MovieState = INITIAL_STATE, action: MovieActions) => {
    const draft = newDraft
    const { type, payload } = action
    switch (type) {
      // ********************* MOVIES *********************
      case MovieTypes.MOVIE_REQUEST:
        draft.moviesLoading = true
        draft.moviesError = false
        draft.moviesErrorMsg = ''
        break

      case MovieTypes.MOVIE_SUCCCES:
        draft.movies = payload.movies
        draft.moviesLoading = false
        draft.moviesError = false
        draft.moviesErrorMsg = ''
        break

      case MovieTypes.MOVIE_ERROR:
        draft.movies = undefined
        draft.moviesLoading = false
        draft.moviesError = true
        draft.moviesErrorMsg = payload.msgError
        break

      case MovieTypes.MOVIE_RESET:
        draft.movies = undefined
        draft.moviesLoading = false
        draft.moviesError = false
        draft.moviesErrorMsg = ''
        break

      // ************************************ ADD MOVIE ************************************
      case MovieTypes.MOVIE_ADD_REQUEST:
        draft.movieAddLoading = true
        draft.movieAddError = false
        draft.movieAddErrorMsg = ''
        break

      case MovieTypes.MOVIE_ADD_SUCCCES:
        draft.movies = payload.movies
        draft.movieAddSuccess = true
        draft.movieAddLoading = false
        draft.movieAddError = false
        draft.movieAddErrorMsg = ''
        break

      case MovieTypes.MOVIE_ADD_ERROR:
        draft.movieAddSuccess = false
        draft.movieAddLoading = false
        draft.movieAddError = true
        draft.movieAddErrorMsg = payload.msgError
        break

      case MovieTypes.MOVIE_ADD_RESET:
        draft.movieAddSuccess = false
        draft.movieAddLoading = false
        draft.movieAddError = false
        draft.movieAddErrorMsg = ''
        break

      // ************************************ Remove MOVIE ************************************
      case MovieTypes.MOVIE_REMOVE_REQUEST:
        draft.movieRemoveLoading = true
        draft.movieRemoveError = false
        draft.movieRemoveErrorMsg = ''
        break

      case MovieTypes.MOVIE_REMOVE_SUCCCES:
        draft.movies = payload.movies
        draft.movieRemoveSuccess = true
        draft.movieRemoveLoading = false
        draft.movieRemoveError = false
        draft.movieRemoveErrorMsg = ''
        break

      case MovieTypes.MOVIE_REMOVE_ERROR:
        draft.movieRemoveSuccess = false
        draft.movieRemoveLoading = false
        draft.movieRemoveError = true
        draft.movieRemoveErrorMsg = payload.msgError
        break

      case MovieTypes.MOVIE_REMOVE_RESET:
        draft.movieRemoveSuccess = false
        draft.movieRemoveLoading = false
        draft.movieRemoveError = false
        draft.movieRemoveErrorMsg = ''
        break

      // ************************************ ADD MOVIE ************************************
      case MovieTypes.MOVIE_EDIT_REQUEST:
        draft.movieEditLoading = true
        draft.movieEditError = false
        draft.movieEditErrorMsg = ''
        break

      case MovieTypes.MOVIE_EDIT_SUCCCES:
        draft.movies = payload.movies
        draft.movieEditSuccess = true
        draft.movieEditLoading = false
        draft.movieEditError = false
        draft.movieEditErrorMsg = ''
        break

      case MovieTypes.MOVIE_EDIT_ERROR:
        draft.movieEditSuccess = false
        draft.movieEditLoading = false
        draft.movieEditError = true
        draft.movieEditErrorMsg = payload.msgError
        break

      case MovieTypes.MOVIE_EDIT_RESET:
        draft.movieEditSuccess = false
        draft.movieEditLoading = false
        draft.movieEditError = false
        draft.movieEditErrorMsg = ''
        break

      // ************************************ Delete Actor Of Movie ************************************
      case MovieTypes.DELETE_ACTOR_MOVIE_REQUEST:
        draft.deleteActorMovieLoading = true
        draft.deleteActorMovieError = false
        draft.deleteActorMovieErrorMsg = ''
        break

      case MovieTypes.DELETE_ACTOR_MOVIE_SUCCESS:
        draft.movies = payload.movies
        draft.deleteActorMovieSuccess = true
        draft.deleteActorMovieLoading = false
        draft.deleteActorMovieError = false
        draft.deleteActorMovieErrorMsg = ''
        break

      case MovieTypes.DELETE_ACTOR_MOVIE_ERROR:
        draft.deleteActorMovieSuccess = false
        draft.deleteActorMovieLoading = false
        draft.deleteActorMovieError = true
        draft.deleteActorMovieErrorMsg = payload.msgError
        break

      case MovieTypes.DELETE_ACTOR_MOVIE_RESET:
        draft.deleteActorMovieSuccess = false
        draft.deleteActorMovieLoading = false
        draft.deleteActorMovieError = false
        draft.deleteActorMovieErrorMsg = ''
        break

      // ************************************ Add Actor Of Movie ************************************
      case MovieTypes.ADD_ACTOR_MOVIE_REQUEST:
        draft.addActorMovieLoading = true
        draft.addActorMovieError = false
        draft.addActorMovieErrorMsg = ''
        break

      case MovieTypes.ADD_ACTOR_MOVIE_SUCCESS:
        draft.movies = payload.movies
        draft.addActorMovieSuccess = true
        draft.addActorMovieLoading = false
        draft.addActorMovieError = false
        draft.addActorMovieErrorMsg = ''
        break

      case MovieTypes.ADD_ACTOR_MOVIE_ERROR:
        draft.addActorMovieSuccess = false
        draft.addActorMovieLoading = false
        draft.addActorMovieError = true
        draft.addActorMovieErrorMsg = payload.msgError
        break

      case MovieTypes.ADD_ACTOR_MOVIE_RESET:
        draft.addActorMovieSuccess = false
        draft.addActorMovieLoading = false
        draft.addActorMovieError = false
        draft.addActorMovieErrorMsg = ''
        break

      // ************************************ Delete Director Of Movie ************************************
      case MovieTypes.DELETE_DIRECTOR_MOVIE_REQUEST:
        draft.deleteDirectorMovieLoading = true
        draft.deleteDirectorMovieError = false
        draft.deleteDirectorMovieErrorMsg = ''
        break

      case MovieTypes.DELETE_DIRECTOR_MOVIE_SUCCESS:
        draft.movies = payload.movies
        draft.deleteDirectorMovieSuccess = true
        draft.deleteDirectorMovieLoading = false
        draft.deleteDirectorMovieError = false
        draft.deleteDirectorMovieErrorMsg = ''
        break

      case MovieTypes.DELETE_DIRECTOR_MOVIE_ERROR:
        draft.deleteDirectorMovieSuccess = false
        draft.deleteDirectorMovieLoading = false
        draft.deleteDirectorMovieError = true
        draft.deleteDirectorMovieErrorMsg = payload.msgError
        break

      case MovieTypes.DELETE_DIRECTOR_MOVIE_RESET:
        draft.deleteDirectorMovieSuccess = false
        draft.deleteDirectorMovieLoading = false
        draft.deleteDirectorMovieError = false
        draft.deleteDirectorMovieErrorMsg = ''
        break

      // ************************************ Add Director Of Movie ************************************
      case MovieTypes.ADD_DIRECTOR_MOVIE_REQUEST:
        draft.addDirectorMovieLoading = true
        draft.addDirectorMovieError = false
        draft.addDirectorMovieErrorMsg = ''
        break

      case MovieTypes.ADD_DIRECTOR_MOVIE_SUCCESS:
        draft.movies = payload.movies
        draft.addDirectorMovieSuccess = true
        draft.addDirectorMovieLoading = false
        draft.addDirectorMovieError = false
        draft.addDirectorMovieErrorMsg = ''
        break

      case MovieTypes.ADD_DIRECTOR_MOVIE_ERROR:
        draft.addDirectorMovieSuccess = false
        draft.addDirectorMovieLoading = false
        draft.addDirectorMovieError = true
        draft.addDirectorMovieErrorMsg = payload.msgError
        break

      case MovieTypes.ADD_DIRECTOR_MOVIE_RESET:
        draft.addDirectorMovieSuccess = false
        draft.addDirectorMovieLoading = false
        draft.addDirectorMovieError = false
        draft.addDirectorMovieErrorMsg = ''
        break

      default:
        return draft
    }
    return newDraft
  }
)

export default moviesReducer
