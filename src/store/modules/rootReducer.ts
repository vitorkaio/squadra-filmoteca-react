import { combineReducers } from 'redux'
import movieReducer from './movies/reducer'
import actorReducer from './actors/reducer'
import directorReducer from './directors/reducer'

export default combineReducers({
  movieReducer,
  actorReducer,
  directorReducer
})
