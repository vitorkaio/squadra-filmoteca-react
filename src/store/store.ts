import { createStore, applyMiddleware, Store } from 'redux'
import createSagaMiddleware from 'redux-saga'
// import logger from 'redux-logger'
// import { composeWithDevTools } from 'redux-devtools-extension'
import { MovieState } from './modules/movies/types'
import { ActorState } from './modules/actors/types'
import { DirectorState } from './modules/directors/types'

import rootReducer from './modules/rootReducer'
import rootSaga from './modules/rootSagas'
import { rootActions } from './modules/rootActions'

export interface ApplicationState {
  movieReducer: MovieState
  actorReducer: ActorState
  directorReducer: DirectorState
}

const sagaMiddleware = createSagaMiddleware()

const store: Store<ApplicationState, rootActions> = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware)
  /* composeWithDevTools(
    applyMiddleware(sagaMiddleware)
    // other store enhancers if any
  ) */
)

sagaMiddleware.run(rootSaga)

export default store
