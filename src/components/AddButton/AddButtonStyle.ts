import styled from 'styled-components'

export const Container = styled.div`
  position: absolute;
  right: 20px;
  bottom: 20px;
  z-index: 99;
`
