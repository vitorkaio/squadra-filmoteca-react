export interface DispatchProps {
  action(): void
}
