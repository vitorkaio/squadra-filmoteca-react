import React from 'react'
import { Fab } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'

import { Container } from './AddButtonStyle'
import { DispatchProps } from './AddButtonTypes'

type Props = DispatchProps

const AddButton: React.FC<Props> = props => {
  const { action } = props
  return (
    <Container>
      <Fab color="primary" aria-label="add" onClick={action} size="medium">
        <AddIcon />
      </Fab>
    </Container>
  )
}

export default AddButton
