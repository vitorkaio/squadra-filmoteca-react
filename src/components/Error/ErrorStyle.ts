import styled from 'styled-components'
import { Colors } from 'components/styles/Colors'

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`
export const TextError = styled.span`
  color: ${Colors.error};
  font-weight: 450;
  text-align: center;
`
