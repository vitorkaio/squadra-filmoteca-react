import React from 'react'

import { Container, TextError } from './ErrorStyle'
import { StateProps } from './ErrorTypes'

type Props = StateProps

const Error: React.FC<Props> = props => {
  const { error } = props
  return (
    <Container>
      <TextError>{error}</TextError>
    </Container>
  )
}

export default Error
