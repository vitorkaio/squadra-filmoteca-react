// Define o estilo global para aplicação
import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }
  
  html, body, #root {
    height: 100%;
  }
  body {
    font: 14px 'Roboto', sans-serif;
    background-color: #fff;
    -webkit-font-smoonthing: antialiased !important;
  }
  ul {
    list-style: none;
  }
`

/* 
  Define que a altura da tela será de 100%, evitando scrollbars desnecessários.
  html, body, #root {
    height: 100%;
  }
*/
