export enum Colors {
  primary = '#9c27b0',
  error = 'coral',
  border = '#e0e0e0',
  text = '#303030'
}
