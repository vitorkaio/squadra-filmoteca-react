import { createMuiTheme } from '@material-ui/core/styles'
import { teal } from '@material-ui/core/colors'

const theme = createMuiTheme({
  palette: {
    primary: teal,
    secondary: {
      main: '#303030'
    }
  }
})

export default theme
