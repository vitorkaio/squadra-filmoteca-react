export interface Director {
  id: number
  name: string
}

export interface DirectorAdd {
  name: string
}
