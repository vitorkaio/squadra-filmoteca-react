import { Actor } from './Actor.model'
import { Director } from './Director.model'

export interface Movie {
  id: number
  name: string
  actors?: Actor[]
  directors?: Director[]
}

export interface MovieAdd {
  name: string
}

export interface MovieEdit {
  id: number
  name: string
}

export interface ManagerActorMovie {
  idMovie: number
  idActor: number
}

export interface ManagerDirectorMovie {
  idMovie: number
  idDirector: number
}
