export interface Actor {
  id: number
  name: string
}

export interface ActorAdd {
  name: string
}
